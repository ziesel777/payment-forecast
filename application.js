/* ----------------------- ГЛОБАЛЬНЫЕ ПАРАМЕТРЫ [start] ----------------------------- */

var todayDate = new Date(); // сегодняшняя дата
todayDate.setHours(23); // устанавливаем время даты
todayDate.setMinutes(59);// устанавливаем время даты
todayDate.setSeconds(58);// устанавливаем время даты

var todayWeekDay = todayDate.getDay(); // сегодняшний день недели
if (todayWeekDay == 0) {
    todayWeekDay = 7;
}

var curDomain = (getUrlVars()["DOMAIN"]), // адрес портала
    mondayDate = new Date(), mondayDateCur = new Date(),
    sundayDate = new Date(), sundayDateCur = new Date(),
    todayDateFrom = new Date(), todayDateTo = new Date(),
    managersCount = 0, invoicesCount = 0, // кол-во менеджеров и кол-во счетов (необходимы для очистки таблицы при нажатии стрелки недели)
    thisWeekSum = '', thisDaySum = '', currentUserID = '', currentUserName = '',
    allManagers = {}, arAllData = {}, prevArguments = [], thisWeekSumObj = {}, arEndSums = {}, arEndSumsUpdate = [],
    prevCount = 1, sumFlag = false, left = 'left', right = 'right',
    fullSumDays = [0, 0, 0, 0, 0, 0, 0], arCorrectionsIds = {}, arCorrections = {}, arFullCorrections = {}, arHistories = {};

thisWeekSumObj = {
    incomes: {
        sumPayed: 0,
        sumNotPayed: 0,
    },
    expenses: {
        sumPayed: 0,
        sumNotPayed: 0,
    },
};

prevArguments[1] = {prevDays:21,dates:'#dates-prev-3',sums:'#sums-prev-3'};
prevArguments[2] = {prevDays:14,dates:'#dates-prev-2',sums:'#sums-prev-2'};
prevArguments[3] = {prevDays:7,dates:'#dates-prev-1',sums:'#sums-prev-1'};
prevArguments[4] = {prevDays:-7,dates:'#dates-next-1',sums:'#sums-next-1'};
prevArguments[5] = {prevDays:-14,dates:'#dates-next-2',sums:'#sums-next-2'};
prevArguments[6] = {prevDays:-21,dates:'#dates-next-3',sums:'#sums-next-3'};

months = ["ЯНВ", "ФЕВ", "МАРТ", "АПР", "МАЙ", "ИЮНЬ", "ИЮЛЬ", "АВГ", "СЕН", "ОКТ", "НОЯ", "ДЕК"];
monthsRusNames = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
];

invoiceItems = {};
invoicesIds = {};
declineStatuses = [];

/*BX24.callMethod('user.current', {}, function(res){
    currentUserID = res.data().ID;
    currentUserName = res.data().NAME + " " + res.data().LAST_NAME;
});*/

/* ----------------------- ГЛОБАЛЬНЫЕ ПАРАМЕТРЫ [end] ----------------------------- */

// для получения URL портала
function getUrlVars() {
    var vars = {},
        parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
    return vars;
}

// Вызов/отмена прелоадера
// application.prototype.toggleLoading = function () {
//     if ($('div.progress-bar-j').is(':visible')) {
//         $('div.progress-bar-j span').css("width", "0%").animate({width: "100%"},1000);
//         $('div.progress-bar-j').fadeOut(0, 'swing');
//     } else {
//         $('div.progress-bar-j span').css("width", "0%").animate({width: "100%"},1000);
//         $('div.progress-bar-j').fadeIn(500, 'swing');
//     }
// };

// application.prototype.addProgressBar = function () {
//     var progressBarWidth = $('div.progress-bar-j span').width();
//     $('div.progress-bar-j span').css("width", progressBarWidth+100);
// };

// Вывод дат ++++++++++++++++++++++++
// 

// Вывод менеджеров, их счетов и оплат, а так же сумм менеджеров
function displayPays(allInvoices, selector, selectorSums) {
    var self = this;
    var paysHTML = '';
    var managerPaySums = [];
    var managerNotPaySums = [];
    var managerSums = [];
    var manInc = 0;
    managersCount = 0;
    invoicesCount = 0;

    allInvoices.sort(function (a, b) {
        if (a[0] > b[0]) {
            return 1;
        } else if (a[0] < b[0]) {
            return -1;
        } else if (a[0] == b[0]) {
            if (a[5] > b[5]) {
                return 1;
            } else if (a[5] < b[5]) {
                return -1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    });

    for (var i=0;i<allInvoices.length;i++) {
        var invoicePathArr = allInvoices[i][2][1].split("/");
        var invoiceId = invoicePathArr[4];
        var invoiceDueDate = false;
        var invoiceDueDateReverse = false;
        var invoiceStatus = allInvoices[i][2][2];
        var tooltipHTML = '';

        // [start] tooltipHTML ///////////////////////////////////////////////////////////////////////////////////////////
        if (arAllData.entityDuedates[invoiceId] !== undefined && arAllData.entityDuedates[invoiceId] !== '') {
            tooltipHTML += '<div>';
            var countDues = 0;
            arEntityDuedates = arAllData.entityDuedates[invoiceId];
            dpDuedateAfter = arEntityDuedates[arEntityDuedates.length - 1].duedateAfter.split('.');
            invoicesIds[invoiceId] = new Date(dpDuedateAfter[1] + '.' + dpDuedateAfter[0] + '.' + dpDuedateAfter[2]);
            $.each(arAllData.entityDuedates[invoiceId], function(i, objDuedate) {
                console.log(objDuedate);
                if (countDues > 0) {
                    tooltipHTML += '<br/><br/>';
                }
                tooltipHTML += '<div class="text-content" style="padding-bottom: 20px;">';
                tooltipHTML += (invoiceStatus != 'payed') ? '<a class="report-date" href="#">Перенести на дату</a>' :'';
                tooltipHTML += '<span class="text-center2"><span class="first-line">'+ objDuedate.user +'</span> <span class="second-line">'+ objDuedate.duedateBefore +'<span class="blue-arrow"> &rarr; </span>'+ objDuedate.duedateAfter +' '+ objDuedate.date +'</span><br/>';
                tooltipHTML += '<span class="text-center3"> → </span>';
                if (objDuedate.comment != '') {
                    tooltipHTML += '<br/><span class="text-center">'+ objDuedate.comment +'</span>';
                }

                countDues++;
            });
            tooltipHTML += '</div>';
        } else {
            tooltipHTML += '<div class="text-content">';
            tooltipHTML += (invoiceStatus != 'payed') ? '<a class="report-date" href="#">Перенести на дату</a>' :'';
            tooltipHTML += '<span class="text-center4">Нет изменений срока оплаты</span></div>';
        }

        // [end] tooltipHTML ///////////////////////////////////////////////////////////////////////////////////////////

        if (allInvoices[i][3] !== undefined && allInvoices[i][3] !== '') {
            invoiceDueDate = tempus(allInvoices[i][3]).format('%d.%m.%Y');
            invoiceDueDateReverse = tempus(allInvoices[i][3]).format('%Y-%m-%d');
        } else { //если нет даты оплаты то ставим сегодня
            invoiceDueDate = tempus().calc({day: 0}).format('%d.%m.%Y');
            invoiceDueDateReverse = tempus().calc({day: 0}).format('%Y-%m-%d');
        }

        if (i == 0) {
            // для первой строчки (первого менеджера и первого счета)
            // вывод первого менеджера
            //paysHTML = "<tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td id='managpay"+ manInc +"'></td><td id='managprepay"+ manInc +"'></td><td id='managsumpay"+ manInc +"'></td><td class='td-hr'></td><td colspan='7'></td></tr>";
            paysHTML = "<tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td class='managpay' id='managpay"+ manInc +"'><div class='managpay number'></div></td><td>+</td><td class='managprepay' id='managprepay"+ manInc +"'><div class='managprepay number'></div></td><td class='td-hr'></td><td colspan='7'></td></tr>";

            // вывод первой оплаты первого менеджера
            //paysHTML += "<tr id='invoice"+ invoicesCount +"'><td class='left' colspan='4'><a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>"
            paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='4'>";
            paysHTML += "<a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";

            // перебор по дням
            var curDate = new Date(mondayDate);
            for (var j=0;j<7;j++) {
                if (allInvoices[i][2][4][j] == 0 || allInvoices[i][2][4][j] == null) { // если в этот день нет оплаты, то пустое поле
                    paysHTML += `<td num-day="${j+1}" date-day="${curDate.getDate()+j}"></td>`;
                } else { // иначе записываем в поле сумму счета и
                    paysHTML += "<td class='" + allInvoices[i][2][2] + "' data-invoiceId='"+ invoiceId +"'";
                    if (tooltipHTML !== '') {
                        paysHTML += " data-duedates='true'";
                    }
                    if (invoiceDueDate) {
                        paysHTML += "><span><a class='edit-duedates click-black' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "<a/></span></td>";
                        createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
                    } else {
                        paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                    }
                }
            }

            // подсчет сумм оплат по менеджеру для первого счета
            if (allInvoices[i][2][2] == 'payed') {
                managerPaySums[manInc] = allInvoices[i][2][3];
                managerNotPaySums[manInc] = 0;
            } else {
                managerNotPaySums[manInc] = allInvoices[i][2][3];
                managerPaySums[manInc] = 0;
            }
            managerSums[manInc] = allInvoices[i][2][3];

            paysHTML += "</tr>";

            managersCount++; // переход на следующего менеджера
            invoicesCount++; // переход на следующий счет

        } else {
            // если предыдущий счет от другого менеджара, то выводим нового менеджера
            if (allInvoices[i][1] != allInvoices[i-1][1]) {
                manInc++;
                managerPaySums[manInc] = 0;
                managerNotPaySums[manInc] = 0;
                managerSums[manInc] = 0;

                // вывод менеджера
                //paysHTML += "</tr><tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td id='managpay"+ manInc +"'></td><td id='managprepay"+ manInc +"'></td><td id='managsumpay"+ manInc +"'></td><td class='td-hr'></td><td colspan='7'></td></tr>";
                paysHTML += "</tr><tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td class='managpay' id='managpay"+ manInc +"'><div class='managpay number'></div></td><td>+</td><td class='managprepay' id='managprepay"+ manInc +"'><div class='managprepay number'></div></td><td class='td-hr'></td><td colspan='7'></td></tr>";
                managersCount++; // переход на следующего менеджера
            }

            // вывод названия счетов и их оплат
            //paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='3'><a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";
            paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='4'>";
            /*if (invoiceDueDate) {
                paysHTML += "<button class='edit-duedates' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>✎</button> ";
                createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
            }*/
            paysHTML += "<a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";
            invoicesCount++; // переход к следующему счету

            // перебор по дням
            let curDate = new Date(mondayDate);
            for (var j=0;j<7;j++) {
                if (allInvoices[i][2][4][j] == 0 || allInvoices[i][2][4][j] == null) { // если в этот день нет оплаты, то пустое поле
                    paysHTML += `<td num-day="${j+1}" date-day="${curDate.getDate()+j}"></td>`;
                } else { // иначе записываем в поле сумму счета и
                    paysHTML += "<td class='" + allInvoices[i][2][2] + "' data-invoiceId='"+ invoiceId +"'";
                    if (tooltipHTML !== '') {
                        paysHTML += " data-duedates='true'";
                    }

                    if (invoiceDueDate) {
                        paysHTML += "><span><a class='edit-duedates click-black' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "<a/></span></td>";
                        createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
                    } else {
                        paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                    }
                    //paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                }
            }

            // подсчет сумм по менеджерам
            if (allInvoices[i][2][2] == 'payed') {
                managerPaySums[manInc] += allInvoices[i][2][3];
            } else {
                managerNotPaySums[manInc] += allInvoices[i][2][3];
            }
            managerSums[manInc] += allInvoices[i][2][3];

            paysHTML += "</tr>";
        }
    }

    // Выделение столбца таблицы текущей даты
    $(`td[date-day="${new Date().getDate()}"]`).css('background-color','rgba(24, 91, 207, 0.1)');
    //---------------------------------------

    $(selector).after(paysHTML);

    // вывод сумм оплаченных счетов менеджера
    for (i=0;i<managerPaySums.length;i++) {
        var man_class = "#managpay"+i;
        $(man_class).find('.number').html(accounting.formatNumber(managerPaySums[i], 0, " "));
    }

    // вывод сумм неоплаченных счетов менеджера
    for (i=0;i<managerNotPaySums.length;i++) {
        var man_class = "#managprepay"+i;
        $(man_class).find('.number').html(accounting.formatNumber(managerNotPaySums[i], 0, " "));
    }
    BX24.fitWindow();
}

// Вывод строки сумм
function displaySums(allInvoices, arExpenses, selectorSums, title, step) {
        console.log(allInvoices);
    var sumDays = [0, 0, 0, 0, 0, 0, 0];

    for (var i=0;i<allInvoices.length;i++) {
        if (allInvoices[i][2][2] == 'payed') {
            sumPayed += allInvoices[i][2][3];
            thisWeekSumObj[step].sumPayed += allInvoices[i][2][3];
        } else {
            sumNotPayed += allInvoices[i][2][3];
            thisWeekSumObj[step].sumNotPayed += allInvoices[i][2][3];
        }

        for (var j=0;j<7;j++) {
            sumDays[j] += allInvoices[i][2][4][j];
        }
    }

    var sumsHTML = "<td class='first-col'>" + title + "</td><td class='sum'></td><td class='sum'></td><td class='td-hr'></td>";

    for (i=0;i<7;i++) {
        if (sumDays[i] != 0) {
            sumsHTML += "<td class='td-days'>"+ accounting.formatNumber(sumDays[i], 0, " ") +"</td>";
        } else {
            sumsHTML += "<td class='td-days'>0</td>";
        }
    }

    $(sumsHTML).appendTo(selectorSums);

    if (sumFlag === false) {
        sumFlag = true;
        displaySums(arExpenses, arExpenses, "#deal-expense", "Расходы", 'expenses'); // вывод верхней строки сумм в таблицу
    } else {
        // вывод верхней строки сумм в таблицу
        $('#deal-income .td-days').each(function (i) {
            var dayIncomesSum = $(this).text().split(' ').join('');
            var dayExpensesSum = $('#deal-expense .td-days:eq('+ i +')').text().split(' ').join('') || 0;
            var daySum = parseInt(dayIncomesSum) + parseInt(dayExpensesSum);

            fullSumDays[i] = daySum;

            $('#deal-sums .td-days:eq('+ i +')').html(accounting.formatNumber(daySum, 0, " "));
        });

        // показ суммы за текущую неделю
        var sumPayed = parseInt(thisWeekSumObj['incomes'].sumPayed) + parseInt(thisWeekSumObj['expenses'].sumPayed);
        var sumNotPayed = parseInt(thisWeekSumObj['incomes'].sumNotPayed) + parseInt(thisWeekSumObj['expenses'].sumNotPayed);

        if (sumPayed == 0) {
            $('#sums-payed').html("<span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()) {
                thisWeekSum = "<span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>";
            }
        } else if (sumNotPayed == 0) {
            $('#sums-payed').html("<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()) {
                thisWeekSum = "<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span>";
            }
        } else {
            $('#sums-payed').html("<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span> + <span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()){
                thisWeekSum = "<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span> + <span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>"
            }
        }

        loadCorrections(mondayDate, allInvoices);
    }

    BX24.fitWindow();
}

/* Корректировки */
function loadCorrections(startDate, pay) {
    var arDates = [tempus(startDate).format('%Y.%m.%d')];
    var modDate = new Date(startDate);

    for (var i=1;i<7;i++) {
        modDate.setDate(modDate.getDate() + 1);
        arDates.push(tempus(modDate).format('%Y.%m.%d'));
    }

    var arCorrections = {};
    var beginHTML = '<td class="text-center" colspan="4">Остаток на начало дня</td><td class="td-hr"></td>';
    // var corrHTML = '<td class="text-center" colspan="4">Скорректированный остаток на начало дня</td><td class="td-hr"></td>';
    var corrHTML = '<td class="text-center" colspan="4">Остаток на начало дня</td><td class="td-hr"></td>';
    var endHTML = '<td class="text-center" colspan="4">Итого</td><td class="td-hr"></td>';
    var modalHTML = "";
    var tooltipHTML = [];
    var sumCorrEnd = 0;
    var prevEndSum = 0;
    var prevEndDate = new Date(mondayDate);
    var calcDate = 1;
    var arCommands = [];
    var curWeekSundayDate = new Date(prevEndDate);

    curWeekSundayDate = tempus(curWeekSundayDate).calc({day: 6}).format('%Y.%m.%d');

    prevEndDate = tempus(prevEndDate).calc({day: -calcDate}).format('%Y.%m.%d');

    $.each(arAllData.entityCorrections, function(index, value){
        if (~arDates.indexOf(value.NAME)) {
            arFullCorrections[value['NAME']] = JSON.parse(value.PREVIEW_TEXT);
            arCorrections[value['NAME']] = arFullCorrections[value['NAME']]['corrSum'];
            arCorrectionsIds[value['NAME']] = value['ID'];
        }

        var objHistory = JSON.parse(value.DETAIL_TEXT);

        if (objHistory != null) {
            // TODO: сделать перебор по объекту objHistory (дни недели) и сгенерировать для каждого дня массив историй
            $.each(objHistory, function (historyDate, arHistory) {
                if (arHistories[value['NAME']] === undefined) {
                    arHistories[value['NAME']] = {};
                }

                if (arHistories[value['NAME']][historyDate] === undefined) {
                    arHistories[value['NAME']][historyDate] = {};
                }

                /*if (arHistories[value['NAME']][datetime] === undefined) {
                    arHistories[value['NAME']][datetime] = {};
                }*/

                $.each(arHistory, function (datetime, history) {
                    arHistories[value['NAME']][historyDate][datetime] = {
                        username: history['username'],
                        sum_before: history['sum_before'],
                        sum_after: history['sum_after'],
                    };
                });
            })
        }

        if (value.NAME == prevEndDate) {
            prevEndSum = JSON.parse(value.PREVIEW_TEXT)['endSum'];
            /*console.log(value);
            console.log('prevEndDate: '+prevEndDate);
            console.log('prevEndSum: '+prevEndSum);*/
        }

        /*if (value.ID == '15689') {
            console.log(arCorrections[value.NAME]);
            console.log(arHistories[value.NAME]);
            console.log('-----------------------');
        }*/
    });

    $.each(arDates,function(index, value){
        //console.log(value+ ': ' +prevEndSum);

        beginHTML += "<td>"+accounting.formatNumber(prevEndSum, 0, " ")+"</td>";

        var daySumCorr = 0;

        if (arCorrections[curWeekSundayDate] === undefined) {
            arCorrections[curWeekSundayDate] = {};
        }

        if (arCorrections[curWeekSundayDate][value] !== '') {
            daySumCorr = parseInt(arCorrections[curWeekSundayDate][value]);
        } else {
            daySumCorr = prevEndSum;
        }
        corrHTML += "<td><a data-toggle='modal' data-target='#corrModal-"+(index+1)+"'>"+accounting.formatNumber(daySumCorr, 0, " ")+"<a/></td>";

        // sumCorrEnd = parseInt(daySumCorr)+parseInt(fullSumDays[index]);
        sumCorrEnd = parseInt(fullSumDays[index]);
        prevEndSum = sumCorrEnd;

        /*if (index == arDates.length-1) {
            //updateEndSum(value, prevEndSum);
            if (prevEndSum < 0) {
                thisWeekSum = "<span class='sum-span corrEndMinus'>" + accounting.formatNumber(prevEndSum, 0, " ") + "</span>";
            } else {
                thisWeekSum = "<span class='sum-span corrEndPlus'>" + accounting.formatNumber(prevEndSum, 0, " ") + "</span>";
            }
            $('#sums-payed').html(thisWeekSum);
        }*/

        var corrEndClass = '';

        if (sumCorrEnd < 0) {
            corrEndClass = 'corrEndMinus';
        } else {
            corrEndClass = 'corrEndPlus';
        }

        //indicatorHTML += "<td class='expanded-corr expanded-corr"+(index+1)+" "+indicatorClass+"'></td>";
        endHTML += "<td class='"+corrEndClass+"'>"+accounting.formatNumber(sumCorrEnd, 0, " ")+"</td>";


        /*if (isAdmin) {
            modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
                "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
                "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
                "<h4 class='modal-title'>Корректировка остатков</h4></div>" +
                "<div class='modal-body text-center' data-date='"+value+"'>";
            if (arCorrections[value] && arCorrections[value][0]) {
                modalHTML += "<div class='text-center sumCorrBefore' data-sumCorrBefore='"+arCorrections[value][0]+"'>Текущая сумма "+arCorrections[value][0]+"</div>";
            }

            for(var i=1;i<8;i++) {
                var inputValue = '';
                if (arCorrections[value] && arCorrections[value][i]) {
                    inputValue = arCorrections[value][i];
                }
                modalHTML += "<label>"+corrStrokes[i-1]+"</label> <input class='text-center inputCorr inputCorr-"+i+"' type='number' value='"+inputValue+"' size='5'><br/>"
            }
            modalHTML += "</div><div class='modal-footer'>" +
                "<button type='button' class='btn btn-success saveCorr' data-dismiss='modal' data-date='"+value+"'>Сохранить</button>" +
                "<button type='button' class='btn' data-dismiss='modal'>Закрыть</button>" +
                "</div></div></div></div>";
        } else {
            modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
                "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
                "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
                "<h4 class='modal-title'>Корректировка остатков</h4></div>" +
                "<div class='modal-body text-center' data-date='"+value+"'>";
            modalHTML += "<div class='text-center'>Корректировки доступны только администраторам приложения.</div>";
            modalHTML += "</div><div class='modal-footer'>" +
                "<button type='button' class='btn' data-dismiss='modal'>Закрыть</button>" +
                "</div></div></div></div>";
        }*/


        modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
            "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
            "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
            "<h4 class='modal-title'>Корректировка остатков на счетах</h4></div>" +
            "<div class='modal-body text-center' data-date='"+value+"'>";

        /*if (arCorrections[value] && arCorrections[value][0]) {
            modalHTML += "<div class='text-center sumCorrBefore' data-sumCorrBefore='"+daySumCorr+"'>Текущая сумма "+daySumCorr+"</div>";
        }*/

        modalHTML += "<label>Сумма</label><input class='text-center inputCorr' type='number' autofocus='autofocus' placeholder='"+accounting.formatNumber(daySumCorr, 0, " ")+"' size='5' data-sumCorrBefore='"+daySumCorr+"'><br/>"

        modalHTML += "</div><div class='modal-footer'>" +
            "<button type='button' class='btn btn-success saveCorr' data-dismiss='modal' data-date='"+value+"'>Перенести</button>" +
            "<button type='button' class='btn' data-dismiss='modal'><span class='line'>Отменить перенос</span></button>" +
            "</div></div></div></div>";


        if (arHistories[curWeekSundayDate] === undefined) {
            arHistories[curWeekSundayDate] = {};
        }

        if (arHistories[curWeekSundayDate][value] !== undefined) {
            var arCount = 0;
            tooltipHTML[index+1] = '';
            $.each(arHistories[curWeekSundayDate][value],function(datetime, value){
                if (arCount != 0) {

                    tooltipHTML[index+1] += "<br/>";
                }
                tooltipHTML[index+1] += "<div class='text-center'>"+value['username']+"<br/>("+datetime+")</div>";
                tooltipHTML[index+1] += "<div class='text-center'>"+value['sum_before']+" &#8594; "+value['sum_after']+"</div>";
                arCount++;
            })
        }
    });

    // $('#balance-begin').append(beginHTML);
    $('#balance-corr').append(corrHTML);
    $('#balance-end').append(endHTML);

    $('body').append(modalHTML);

    for (var j = 1; j < 8; j++) {
        $('td a[data-target="#corrModal-' + parseInt(j) + '"]').tooltip({
            title: tooltipHTML[j],
            html: true,
            placement: "left"
        });
    }

    if (BX24.appOption.get("restart") === 'Y') {
        BX24.appOption.set("restart", "N", function () {
            BX24.reloadWindow();
        });
    } else {
        app.addTooltipsToDuedates(pay);
    }
}

function createDuedateModal(invoiceId, invoiceDuedate, invoiceDuedateReverse) {
    var modalHTML = '' +
        // '<div class="modal fade" id="duedate-'+ invoiceId +'" role="dialog">' +
            // '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                       '<button type="button" class="btn" data-dismiss="modal"><span class="line">Отменить перенос</span></button>' +
                        // '<h4 class="modal-title">Изменение срока оплаты счета №'+ invoiceId +'</h4>' +
                    '</div>' +
                    '<div class="modal-body text-center">' +
                        // '<input class="text-center duedate-input" type="text" value="'+ invoiceDuedate +'" disabled="disabled">' +
                        // '<span> &#8594; </span>' +
                        '<div class="text-center datepicker-here duedate-input duedate-input-'+ invoiceId +'"></div>' +
                        // '<input type="text" class="text-center datepicker-here duedate-input duedate-input-'+ invoiceId +'" data-invoiceid="'+ invoiceId +'" readonly="readonly"><br/>' +
                        '<textarea class="duedate-textarea duedate-textarea-'+ invoiceId +'" data-invoiceid="'+ invoiceId +'" rows="2" placeholder="Введите ваш комментарий..."></textarea>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-success saveDuedate saveDuedate-'+ invoiceId +'" data-dismiss="modal" data-duedate="'+ invoiceDuedate +'" data-invoiceid="'+ invoiceId +'" disabled="disabled">Перенести</button>' +

                    '</div>' +
                // '</div>' +
            // '</div>' +
        '</div>';

    // $('div.modals').append(modalHTML);
    invoiceItems[invoiceId] = modalHTML;
}

$(document).on("input", ".duedate-textarea", function () {
    var invoiceId = $(this).data('invoiceid');
    var inputVal = $('.duedate-input-'+invoiceId).val();
    var textareaVal = $('.duedate-textarea-'+invoiceId).val();
    if (inputVal !== '' && textareaVal !== '') {
        $('.saveDuedate-'+invoiceId).prop('disabled', false);
    } else {
        $('.saveDuedate-'+invoiceId).prop('disabled', true);
    }
});

$(document).on("blur", ".duedate-input, .datepicker-here", function () {
    var invoiceId = $(this).data('invoiceid');
    var inputVal = $('.duedate-input-'+invoiceId).val();
    var textareaVal = $('.duedate-textarea-'+invoiceId).val();
    if (inputVal !== '' && textareaVal !== '') {
        $('.saveDuedate-'+invoiceId).prop('disabled', false);
    } else {
        $('.saveDuedate-'+invoiceId).prop('disabled', true);
    }
});

$(document).on("click", "button.saveDuedate", function () {
    var invoiceId = $(this).data('invoiceid');
    var curDatetime = tempus().format('%d.%m.%Y %H:%M');
    var oldDuedate = $(this).data('duedate');
    var newDuedateReverse = tempus($('.duedate-input-'+invoiceId).val()).format('%Y-%m-%d');
    var newDuedate = new Date(newDuedateReverse);
    newDuedate.setHours(5);
    var comment = $('.duedate-textarea-'+invoiceId).val();

    var fields = {
        DATE_PAY_BEFORE: newDuedate
    };

    if (oldDuedate !== tempus(newDuedate).format('%d.%m.%Y')) {
        BX24.callMethod(
            'crm.invoice.update',
            {"id": invoiceId, "fields": fields},
            function(result)
            {

                if(result.error()){
                    console.error(result.error());
                    var errorData = result.error();
                    alert('Ошибка: ' + errorData.ex.error_description.replace(/<[^>]+>/g,''));
                }
                else
                {
                    BX24.callMethod('entity.item.add', {
                        ENTITY: 'duedates',
                        NAME: invoiceId,
                        PROPERTY_VALUES: {
                            date: curDatetime,
                            duedateAfter: tempus(newDuedate).format('%d.%m.%Y'),
                            duedateBefore: oldDuedate,
                            user: currentUserName,
                            comment: comment,
                        },
                    }, function (result) {
                        if(result.error())
                            console.error(result.error());
                        else
                        {
                            location.reload();
                        }
                    });
                }
            }
        );
    }
});

// сохранить корректировку
$('body').on("click", "button.saveCorr", function () {
    var dateCorr = $(this).data('date');
    var input = $('div.modal-body[data-date="'+dateCorr+'"]').find("input");
    var sumCorr = parseInt(input.val());
    if (isNaN(sumCorr) && sumCorr != 0) {
        sumCorr = '';
    }

    var sumCorrBefore = input.data("sumcorrbefore");
    var datetime = tempus().format("%d.%m.%Y %H:%M:%S");

    // дата воскресенья на обрабатываемой неделе
    var curDate = new Date(dateCorr);
    var CurDay = curDate.getDay();
    var curWeekSundayDate = dateCorr;

    if (CurDay != 0) {
        var daysToSunday = 7 - parseInt(CurDay);
        curDate.setDate(curDate.getDate() + daysToSunday);
        curWeekSundayDate = tempus(curDate).format('%Y.%m.%d');
    }

    var corrId = arCorrectionsIds[curWeekSundayDate];

    var arCommands = [];
    var valueCorr = 0;
    var objQuery = {};

    if (!arHistories.hasOwnProperty(curWeekSundayDate)) {
        arHistories[curWeekSundayDate] = {};
    }

    if (!arHistories[curWeekSundayDate].hasOwnProperty(dateCorr)) {
        arHistories[curWeekSundayDate][dateCorr] = {};
    }

    arHistories[curWeekSundayDate][dateCorr][datetime] = {
        username: currentUserName,
        sum_before: sumCorrBefore,
        sum_after: sumCorr,
    };

    arCorrections[curWeekSundayDate][dateCorr] = sumCorr;
    arFullCorrections[curWeekSundayDate]['corrSum'] = arCorrections[curWeekSundayDate];

    var corrSums = JSON.stringify(arFullCorrections[curWeekSundayDate]);
    var corrHistory = JSON.stringify(arHistories[curWeekSundayDate]);

    if (corrId) {
        // update
        objQuery = {
            method: 'entity.item.update',
            params: {
                ENTITY: 'corrections',
                ID: corrId,
                PREVIEW_TEXT: corrSums, // сериализованный массив корректировок за неделю
                DETAIL_TEXT: corrHistory, // сериализованный массив истории корректировок за неделю
            }
        };
    } else {
        // add
        objQuery = {
            method: 'entity.item.add',
            params: {
                ENTITY: 'corrections',
                NAME: curWeekSundayDate,
                PREVIEW_TEXT: corrSums, // сериализованный массив корректировок за неделю
                DETAIL_TEXT: corrHistory, // сериализованный массив истории корректировок за неделю
            }
        };
    }

    arCommands.push(objQuery);

    // колбэк перезагружает страницу
    var batchCallback = function(batchResult) {
        BX24.appOption.set("restart", "Y", function () {
            console.log('status: '+BX24.appOption.get("restart"));
            BX24.reloadWindow();
        });
    };

    BX24.callBatch(arCommands, batchCallback);
});


var date2str = function(d)
{
    return d.getFullYear() + '-' + paddatepart(1 + d.getMonth()) + '-' + paddatepart(d.getDate()) + 'T' + paddatepart(d.getHours())
        + ':' + paddatepart(d.getMinutes()) + ':' + paddatepart(d.getSeconds()) + '+03:00';
};
var paddatepart = function(part)
{
    return part >= 10 ? part.toString() : '0' + part.toString();
};

// promises function
function chain(callback) {
    var queue = [];

    function _next() {
        var cb = queue.shift();
        if (cb) {
            cb(_next);
        }
    }

    setTimeout(_next, 0);

    var then = function(cb) {
        queue.push(cb);
        return { then: then }
    }

    return then(callback);
}

// create our application
app = new Payments();


//Скрытие окон по нажатию на область экрана
$(document).mouseup(function (e){ // событие клика по веб-документу
    var el = $('td[data-invoiceid] .tooltip-inner');
    if (el.length) {
        var attr = el.parents('td[data-invoiceid]').attr('data-invoiceid');
        var span = $('td[data-invoiceid="'+attr+'"] span'); // тут указываем ID элемента
        var tooltip = $('td[data-invoiceid="'+attr+'"] .tooltip'); // тут указываем ID элемента
        if (!span.is(e.target) // если клик был не по нашему блоку
            && span.has(e.target).length === 0
            && !tooltip.is(e.target) // если клик был не по нашему блоку
            && tooltip.has(e.target).length === 0) { // и не по его дочерним элементам
            span.tooltip('hide'); // скрываем его
        }

    }
});


//Переключение между комментарием и календарем
$(document).on('click', '.tooltip-inner a.report-date', function(e){
    // console.log('report-date');
    e.preventDefault();
    var parent = $(this).parents('.tooltip-inner')
    parent.find('> *:not(.modal-content)').hide();
    parent.find('.modal-content').show();


    BX24.fitWindow();

});

$(document).on('click', '.tooltip-inner span.line', function(e){
    // console.log('span.line');
    e.preventDefault();
    var parent = $(this).parents('.tooltip-inner')
    parent.find('> *:not(.modal-content)').show();
    parent.find('.modal-content').hide();


    BX24.fitWindow()
});


