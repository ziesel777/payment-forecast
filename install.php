<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bitrix24.css">

    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="//api.bitrix24.com/api/v1/"></script>
    <script type="text/javascript" src="js/consts.js"></script>

</head>
<body>

</body>
<script>

    $(document).ready(function () {
        BX24.init(function () {

            // Константы приложения в consts.js

            $.ajax({
                url: 'https://szdl.ru/install-redirect/.ajax/getInstaller.php',
                type: 'GET',
                data: {
                    'APP_NAME': appName,
                    'APP_NAME_ENG': appNameEng,
                    'APP_CODE_NAME': appCodeName,
                },
                contentType: 'application/json'
            }).done(function (res) {
                if (res && (res !== " ")) {
                    let data = $.parseJSON(res);
                    if (data !== null && data != false && data !== undefined && data.hasOwnProperty('TEXT')) {
                        $('body').html(data.TEXT);
                    }
                }
            }).fail(function (res) {
                console.log('Ошибка при получении инсталлятора: ' + res);
            });
        });
    });
</script>
</html>