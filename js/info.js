$(function () {

    /**
     * Отправляем данные приложения, и для приложения получаем код запроса верстки блока информера
     */

    $.ajax({
        url: 'https://szdl.ru/informer-redirect/.ajax/requestInformer.php',
        type: 'GET',
        contentType: 'application/json'
    }).done(function (res) {
        if (res && (res !== " ")) {
            let data = $.parseJSON(res);
            if ((data !== null && data != false && data !== undefined) && (data.hasOwnProperty('TEXT'))) {

                var s = document.createElement("script");
                s.type = "text/javascript";
                s.innerHTML = data.TEXT;
                $("body").append(s);
            }
        }
    }).fail(function (res) {
        console.log('Ошибка при получении блока информера');
    });
});
