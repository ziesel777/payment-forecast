<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="css/datepicker.min.css" type="text/css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet" />
    <link href="css/bootstrap-theme.min.css" type="text/css" rel="stylesheet" />
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- <link href="css/loader.css" type="text/css" rel="stylesheet" /> -->
    <link rel="stylesheet" href="css/modules/preloader.css">
    <link href="css/application.css?v=<?= rand() ?>" type="text/css" rel="stylesheet" />

    <script type="text/javascript" src="//api.bitrix24.com/api/v1/"></script>
    <script type="text/javascript" src="js/consts.js"></script>

</head>

<body>

    <!-- <table class="table table-bordered table-responsive">
    <tr class="invoice-tr">
        <td class="pre-payed" data-invoiceid="4785" data-duedates="true">
            <span data-original-title="" title="">
                <a class="edit-duedates click-black" data-invoiceid="4785" data-curduedate="31.05.2019">30 750</a>
                <a></a>
            </span>
            <div class="tooltip fade bottom in" style="top: 523px; left: 515px; display: block;">
                <div class="tooltip-arrow" style="left: 59.4203%;"></div>
                <div class="tooltip-inner" style="padding-bottom: 20px;">
                    <div>
                        <a class="report-date">Перенести на дату</a>
                        <span class="text-center">
                            <span class="first-line">30.04.2019 09:21 (Андрей Васильев)</span>
                            <span class="second-line">31.05.2019<span class="blue-arrow">→</span>30.05.2019 (тест комментария)</span>
                            <span class="text-center">тест смены срока обратно</span>
                        </span>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table> -->


    <header id="ajax-informer-top">

    </header>

    <section id="locker-info" style="display: none;">

    </section>
    <div class="flex-container">
        <div class="sidebar sidebar-left clearfix" id="ajax-informer-left">

        </div>

        <div id="app" class="container-fluid">
            <div id="week" style="display: none;">
                <div class="status">
                    <ul>
                        <li style="list-style-image: url(./img/green.png); padding-right: 10px;">Оплата поступила</li>
                        <li style="list-style-image: url(./img/gray.png); padding-right: 10px;">Оплата запланирована</li>
                        <li style="list-style-image: url(./img/red.png); padding-right: 10px;">Оплата просрочена</li>
                    </ul>
                </div>
                <div class="dates">
                    <div class="lock-container">
                        <div class="lock js-lock"><i class="fas fa-lock lock-sign"></i>Итоги по месяцам</div>
                    </div>
                    <table class="table table-responsive dates-table">
                        <tbody>
                            <tr>
                                <td width="*"></td>
                                <td class="months-td" id="sums-month-prev-1" width="1%"></td>
                                <td class="months-td" id="sums-month-payed" width="1%"></td>
                                <td class="months-td" id="sums-month-next-1" width="1%"></td>
                                <td width="*"></td>
                            </tr>
                            <tr>
                                <td width="*"></td>
                                <td class="months-td" id="dates-month-prev-1" width="10%"></td>
                                <td class="months-td" id="date-month-from" width="10%"><span class="todayWeek"><b></b></span></td>
                                <td class="months-td" id="dates-month-next-1" width="10%"></td>
                                <td width="*"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <div class="dates">
                    <table class="table table-responsive dates-table">
                        <tbody>
                            <tr>
                                <td width="*"></td>
                                <td class="dates-td" id="sums-prev-3" width="1%"></td>
                                <td class="dates-td" id="sums-prev-2" width="1%"></td>
                                <td class="dates-td" id="sums-prev-1" width="1%"></td>
                                <td class="dates-td" id="sums-payed" width="1%"></td>
                                <td class="dates-td" id="sums-next-1" width="1%"></td>
                                <td class="dates-td" id="sums-next-2" width="1%"></td>
                                <td class="dates-td" id="sums-next-3" width="1%"></td>
                                <td width="*"></td>
                            </tr>
                            <tr>
                                <td width="*"></td>
                                <td class="dates-td" id="dates-prev-3" width="1%"></td>
                                <td class="dates-td" id="dates-prev-2" width="1%"></td>
                                <td class="dates-td" id="dates-prev-1" width="1%"></td>
                                <td class="dates-td" id="date-from" width="1%"></td>
                                <td class="dates-td" id="dates-next-1" width="1%"></td>
                                <td class="dates-td" id="dates-next-2" width="1%"></td>
                                <td class="dates-td" id="dates-next-3" width="1%"></td>
                                <td width="*"></td>
                            </tr>
                        </tbody>
                    </table>

                </div>


                <table class="table table-bordered table-responsive">
                    <tbody id="deal-list">
                        <!-- Вывод суммы счетов по всему дню -->
                        <!-- <tr id="deal-sums">
                    <td class="first-col"></td>
                    <td class="sum"></td>
                    <td class="sum"></td>
                    <td class="td-hr"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                    <td class="td-days"></td>
                </tr> -->

                        <!-- Вывод дат -->
                        <tr id="deal-dates" class="text-center"></tr>

                        <!-- Вывод остатков на начало дня -->
                        <!--  <tr id="balance-begin" class="text-center"></tr>
                <tr id="balance-corr" class="text-center"></tr> -->

                        <!-- Вывод менеджеров, их счетов и оплат -->
                        <tr id="deal-income" class="text-center income"></tr>


                        <!--  <tr id="deal-expense" class="text-center income"></tr> -->

                        <!-- Вывод остатков на конец дня -->  
                        <tr id="balance-end" class="text-center"></tr>
                    </tbody>
                </table>


                <!-- <div class="empty"></div> -->
                <!-- <div class="empty"></div> -->
            </div>

            <div class="empty"></div>
        </div>


        <div class="sidebar sidebar-right clearfix" id="ajax-informer-right">

        </div>
    </div>

    <div class="progress-bar-j orange shine">
        <span style="width: 10%"></span>
    </div>

    <div class="modals">

        <div class="modal fade lock-popup" id="lock-popup" role="dialog">


        </div>
    </div>
    <div class="ajax-informer"></div>
    
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/datepicker.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/accounting.min.js"></script>
    <script type="text/javascript" src="js/tempusjs.min.js"></script>
    <script type="text/javascript" src="js/restCall.js"></script>
    <script src="js/modules/pace.min.js"></script>
    <script src="js/modules/payments.js?v=<?= rand() ?>"></script>
    <script type="text/javascript" src="application.js?v=<?= rand() ?>"></script>

    <script type="text/javascript">
        // prelaoader - прогресс бар
        Pace.start();

        //перезагрузка страницы Ctrl+Enter
        $(window).keydown(function (e) {
        if (e.ctrlKey && e.keyCode == 13) {
            location.reload();
        }
        });

        $(function() {
            BX24.init(function() {

                /*BX24.callMethod('entity.delete', {'ENTITY': 'duedates'});
                BX24.appOption.set("entityDuedates", "N");*/
                if (BX24.appOption.get("entityCorrections") !== "Y") {
                    BX24.callMethod(
                        'entity.add', {
                            'ENTITY': 'corrections',
                            'NAME': 'corrections',
                            'ACCESS': {
                                AU: 'W'
                            }
                        },
                        function(result) {
                            if (result.error()) {
                                if (result.error().status === 400 || result.error().status === 404) {
                                    BX24.appOption.set("entityCorrections", "Y");
                                }
                            } else {
                                let data = result.data();
                                if (data === true) {
                                    console.log('хранилище corrections установлено');
                                    BX24.appOption.set("entityCorrections", "Y");
                                }
                            }
                        }
                    );
                }

                if (BX24.appOption.get("entityDuedates") !== "Y") {
                    BX24.callMethod(
                        'entity.add', {
                            'ENTITY': 'duedates',
                            'NAME': 'duedates',
                            'ACCESS': {
                                AU: 'W'
                            }
                        },
                        function(result) {
                            if (result.error()) {
                                if (result.error().status === 400 || result.error().status === 404) {
                                    BX24.appOption.set("entityDuedates", "Y", function() {
                                        startApp();
                                    });
                                }
                            } else {
                                data = result.data();
                                if (data === true) {
                                    BX24.callMethod('entity.item.property.add', {
                                        ENTITY: 'duedates',
                                        PROPERTY: 'user',
                                        NAME: 'Менеджер',
                                        TYPE: 'S'
                                    });
                                    BX24.callMethod('entity.item.property.add', {
                                        ENTITY: 'duedates',
                                        PROPERTY: 'date',
                                        NAME: 'Дата изменения',
                                        TYPE: 'S'
                                    });
                                    BX24.callMethod('entity.item.property.add', {
                                        ENTITY: 'duedates',
                                        PROPERTY: 'duedateBefore',
                                        NAME: 'Срок оплаты до',
                                        TYPE: 'S'
                                    });
                                    BX24.callMethod('entity.item.property.add', {
                                        ENTITY: 'duedates',
                                        PROPERTY: 'duedateAfter',
                                        NAME: 'Срок оплаты после',
                                        TYPE: 'S'
                                    });
                                    BX24.callMethod('entity.item.property.add', {
                                        ENTITY: 'duedates',
                                        PROPERTY: 'comment',
                                        NAME: 'Комментарий',
                                        TYPE: 'S'
                                    });

                                    BX24.appOption.set("entityDuedates", "Y");

                                    startApp();
                                }
                            }
                        }
                    );
                } else {
                    startApp();

                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    // стираем хранилище [START] TEST
                    ////////////////////////////////////////////////////////////////////////////////////////////////

                    /*BX24.callMethod(
                        'entity.delete',
                        {'ENTITY': 'corrections'},
                        function () {
                            BX24.callMethod(
                                    'entity.add',
                                    {'ENTITY': 'corrections', 'NAME': 'corrections', 'ACCESS': {AU:'W'}},
                                    function () {
                                        console.log('хранилище пересоздано');
                                    }
                                );
                        }
                    );*/

                    ////////////////////////////////////////////////////////////////////////////////////////////////
                    // стираем хранилище [END] TEST
                    ////////////////////////////////////////////////////////////////////////////////////////////////
                }
            });
        });

        function startApp() {
            mondayDate.setDate(todayDate.getDate() - todayWeekDay + 1); // дата понедельника на этой неделе
            mondayDate.setHours(0); // устанавливаем время даты (часы на 00)
            mondayDate.setMinutes(0); // устанавливаем время даты (минуты на 00)
            mondayDate.setSeconds(1); // устанавливаем время даты (секунды на 01)
            mondayDateCur = new Date(mondayDate);

            sundayDate.setDate(todayDate.getDate() + (7 - todayWeekDay)); // дата воскресенья на этой неделе
            sundayDate.setHours(23); // устанавливаем время даты (часы на 23)
            sundayDate.setMinutes(59); // устанавливаем время даты (минуты на 59)
            sundayDate.setSeconds(59); // устанавливаем время даты (секунды на 59)
            sundayDateCur = new Date(sundayDate);

            app.displayDates(mondayDate, sundayDate, '#deal-dates');

            app.start();
            BX24.fitWindow();
        }
    </script>

    <!-- скрипт открытой линии -->
    <script data-skip-moving="true">
        (function(w, d, u) {
            let s = d.createElement('script');
            s.async = 1;
            s.src = u + '?' + (Date.now() / 60000 | 0);
            let h = d.getElementsByTagName('script')[0];
            h.parentNode.insertBefore(s, h);
        })(window, document, 'https://cdn.bitrix24.ru/teamr/crm/site_button/loader_10_se8vam.js');
    </script>

    <footer id="ajax-informer-bottom" style="clear:both;">

    </footer>

</body>
<script src="js/info.js"></script>
<div id="ajax-informer-legacy"></div>
</body>

</html>