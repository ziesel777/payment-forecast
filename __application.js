

// our application constructor
function application () {
    this.active = false;
}

/* ----------------------- ГЛОБАЛЬНЫЕ ПАРАМЕТРЫ [start] ----------------------------- */

var todayDate = new Date(); // сегодняшняя дата
todayDate.setHours(23); // устанавливаем время даты
todayDate.setMinutes(59);// устанавливаем время даты
todayDate.setSeconds(58);// устанавливаем время даты

var todayWeekDay = todayDate.getDay(); // сегодняшний день недели
if (todayWeekDay == 0) {
    todayWeekDay = 7;
}

var curDomain = (getUrlVars()["DOMAIN"]), // адрес портала
    mondayDate = new Date(), mondayDateCur = new Date(),
    sundayDate = new Date(), sundayDateCur = new Date(),
    todayDateFrom = new Date(), todayDateTo = new Date(),
    managersCount = 0, invoicesCount = 0, // кол-во менеджеров и кол-во счетов (необходимы для очистки таблицы при нажатии стрелки недели)
    thisWeekSum = '', thisDaySum = '', currentUserID = '', currentUserName = '',
    allManagers = {}, arAllData = {}, prevArguments = [], thisWeekSumObj = {}, arEndSums = {}, arEndSumsUpdate = [],
    prevCount = 1, sumFlag = false, left = 'left', right = 'right',
    fullSumDays = [0, 0, 0, 0, 0, 0, 0], arCorrectionsIds = {}, arCorrections = {}, arFullCorrections = {}, arHistories = {};

thisWeekSumObj = {
    incomes: {
        sumPayed: 0,
        sumNotPayed: 0,
    },
    expenses: {
        sumPayed: 0,
        sumNotPayed: 0,
    },
};

prevArguments[1] = {prevDays:21,dates:'#dates-prev-3',sums:'#sums-prev-3'};
prevArguments[2] = {prevDays:14,dates:'#dates-prev-2',sums:'#sums-prev-2'};
prevArguments[3] = {prevDays:7,dates:'#dates-prev-1',sums:'#sums-prev-1'};
prevArguments[4] = {prevDays:-7,dates:'#dates-next-1',sums:'#sums-next-1'};
prevArguments[5] = {prevDays:-14,dates:'#dates-next-2',sums:'#sums-next-2'};
prevArguments[6] = {prevDays:-21,dates:'#dates-next-3',sums:'#sums-next-3'};

months = ["ЯНВ", "ФЕВ", "МАРТ", "АПР", "МАЙ", "ИЮНЬ", "ИЮЛЬ", "АВГ", "СЕН", "ОКТ", "НОЯ", "ДЕК"];
monthsRusNames = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
];

invoiceItems = {};
invoicesIds = {};
declineStatuses = [];

/*BX24.callMethod('user.current', {}, function(res){
    currentUserID = res.data().ID;
    currentUserName = res.data().NAME + " " + res.data().LAST_NAME;
});*/

/* ----------------------- ГЛОБАЛЬНЫЕ ПАРАМЕТРЫ [end] ----------------------------- */

// для получения URL портала
function getUrlVars() {
    var vars = {},
        parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
    return vars;
}

// Вызов/отмена прелоадера
application.prototype.toggleLoading = function () {
    if ($('div.progress-bar-j').is(':visible')) {
        $('div.progress-bar-j span').css("width", "0%").animate({width: "100%"},1000);
        $('div.progress-bar-j').fadeOut(0, 'swing');
    } else {
        $('div.progress-bar-j span').css("width", "0%").animate({width: "100%"},1000);
        $('div.progress-bar-j').fadeIn(500, 'swing');
    }
};

application.prototype.addProgressBar = function () {
    var progressBarWidth = $('div.progress-bar-j span').width();
    $('div.progress-bar-j span').css("width", progressBarWidth+100);
};

// Вывод дат ++++++++++++++++++++++++
application.prototype.displayDates = function(mondayDate, sundayDate, selector) {

    // вывод даты "ОТ" сверху таблицы
    var dateFrom = mondayDate.getDate();
    if (dateFrom < 10) {
        dateFrom = "0"+dateFrom;
    }
    var monthFrom = mondayDate.getMonth()+1;
    if (monthFrom < 10) {
        monthFrom = "0"+monthFrom;
    }

    // вывод даты "ДО" сверху таблицы
    var dateUpto = sundayDate.getDate();
    if (dateUpto < 10) {
        dateUpto = "0"+dateUpto;
    }
    var monthUpto = sundayDate.getMonth();

  // $(selectorDays).html("<span class="todayWeek"><b>("+prevDays+")'>"+dateFrom+" - "+dateUpto+" "+months[monthUpto]+"<b></span>");

    if (todayDate > mondayDate && todayDate < sundayDate) {
        //$('#date-from').addClass('today');
        $('#date-from').html("<span class='todayWeek'>"+dateFrom+"-"+dateUpto+" "+months[monthUpto]+"</span>");
    } else {
        //$('#date-from').removeClass('today');
        $('#date-from').html(dateFrom+"-"+dateUpto+" "+months[monthUpto]);
    }

    var dateHTML = '<th class="left"></th><th class="text-center managpay">Факт</th><th></th><th class="text-center managprepay">План</th><!--<th class="text-center" id="th-sum-all"></th>--><th class="td-hr"></th>';

    var curDate = new Date(mondayDate);

    var days = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

    for (var i=0; i<7; i++) {

        var curDay = curDate.getDate();
        if (curDay < 10) {
            curDay = "0"+curDay;
        }
        var curMonth = curDate.getMonth()+1;
        if (curMonth < 10) {
            curMonth = "0"+curMonth;
        }

        if (curDate.getDate() == todayDate.getDate() && curDate.getMonth() == todayDate.getMonth() && curDate.getFullYear() == todayDate.getFullYear()) {
            dateHTML += '<th class="today text-center"> <span>' + curDay + ' </span><span class="day">' + days[curDate.getDay()] + '</span></th>';
        } else if (curDate.getDay() == 0 || curDate.getDay() == 6) {
            dateHTML += '<th class="day-off text-center">' + curDay + ' <span>' + days[curDate.getDay()] + '</span></th>';
        } else {
            dateHTML += '<th class="text-center">' + curDay + ' <span class="text">' + days[curDate.getDay()] + '</span></th>';
        }

        curDate.setDate(curDate.getDate()+1);
    }

    $(dateHTML).appendTo(selector);

};
//+++++++++++++++++++
application.prototype.getWarningMessage = function(date) {
    console.log('date '+date);
	var diff = tempus(date, '%d.%m.%Y').between(tempus(), 'day');
	var html = '';
	if(diff < 0) {
        html = '<i class="fas fa-lock"></i>&nbsp;До конца бесплатного использования ' + -1*diff + ' дней';
		html += '<br/><a class="js-lock" href="#">Продлить</a>';
	} else {
        html = '<a class="js-lock" href="#"><i class="fas fa-lock"></i>&nbsp;Открыть дополнительные возможности</a>';
    }
    return html;
};
//+++++++++++++++++++
application.prototype.start = function() {
    var self = this;
	$.ajax({
		url: 'https://szdl.ru/app/clientLifetime/app-locker/check.php?DOMAIN='+curDomain,
		type: 'GET',
		contentType: 'application/json'
	}).done(function (res) {
		if (res && (res !== " ")) {
			let data = res;
			var warningMessage = '';
			if ((data !== null && data != false && data !== undefined) && (data.hasOwnProperty('RESULT'))) {
				if (data.RESULT.ACTIVE == 'Y') {
					self.active = true;
				}
				warningMessage = self.getWarningMessage(data.RESULT.DATE);
			}
			//BX24.appOption.set("demoPeriod", tempus().calc({day:-7}).format('%d.%m.%Y'));
            if(!self.active && !data.RESULT.DATE) {
                var demo = BX24.appOption.get("demo");
                var demoPeriod = BX24.appOption.get("demoPeriod");
				warningMessage = self.getWarningMessage(demoPeriod);
                // console.log('demo '+ demo);
                 console.log('period '+ demoPeriod);
                if(demo != 'Y') {
                    BX24.appOption.set("demoPeriod", tempus().calc({day:+7}).format('%d.%m.%Y'));
                    BX24.appOption.set("demo", 'Y');
                    demoPeriod = BX24.appOption.get("demoPeriod");
                    warningMessage = self.getWarningMessage(demoPeriod);
                    self.active = true;
                } else {
                    //BX24.appOption.set("demoPeriod", tempus().calc({day:-7}).format('%d.%m.%Y'));
                    //проверяем не прошла ли дата
                    //если прошла - ставим признак что прошел демопериод и снимаем флаг активности
                    var diff = tempus(demoPeriod, '%d.%m.%Y').between(tempus(), 'day');
                    self.active = (diff <= 0);
                }
            }
            $('#locker-info').append(warningMessage);
            self.getInvoiceStatuses();

		}
	}).fail(function (res) {
		console.log('Ошибка при проверке активности компании');
	});


};

/** Выборка статусов отказа у счетов, статусы отказа возвращаются последовательно по сортировке
 * сразу после статуса с ID="D" ++++++++++++++++++++++++
 * */
application.prototype.getInvoiceStatuses = function() {
    var self = this;
    bDeclinestart = false;
    BX24.callMethod(
        "crm.invoice.status.list",
        {
            "order": { "SORT": "ASC" },
            "filter": {},
            "select": [ "ID", "NAME", "SORT", "SYSTEM" ]
        },
        function(result)
        {
            if(result.error())
                console.error(result.error());
            else
            {
                var statuses = result.data();
                for(var ind in statuses) {
                    var status = statuses[ind];
                    if (status.STATUS_ID == 'D') {
                        bDeclinestart = true;
                    }
                    if (bDeclinestart) {
                        declineStatuses.push(status.STATUS_ID);
                    }
                }
                if(result.more()) {
                    result.next();
                } else {
                    declineStatuses.push("P");
                    self.getAllData();
                }
            }
        }
    );
};

// Загрузка всех данных батчзапросом+++++++++++++++++
application.prototype.getAllData = function () {
    var startDateInvoice = new Date(todayDate);
    startDateInvoice.setDate(1);
    tempus(startDateInvoice).calc({month:-3});
    var endDateInvoice = new Date(startDateInvoice);
    tempus(endDateInvoice).calc({month:12});
    tempus(endDateInvoice).calc({day:-1});

    if (startDateInvoice.getDay() == 0) {
        tempus(startDateInvoice).calc({day:-7});
    } else {
        tempus(startDateInvoice).calc({day:-startDateInvoice.getDay()});
    }

    if (endDateInvoice.getDay() != 0) {
        tempus(endDateInvoice).calc({day:(7-endDateInvoice.getDay())});
    }

    let call = new RestCall();
    //call.init();

    var arCommands = {
        entityDuedates: {
            method: 'entity.item.get',
            params: {
                entity: 'duedates',
                sort: {NAME: 'ASC', ID: 'DESC'},
            }
        },
        entityCorrections: {
            method: 'entity.item.get',
            params: {
                entity: 'corrections',
                sort: {NAME: 'ASC', ID: 'ASC'},
            }
        },
        user: {
            method: 'user.get',
            params: {
                filter: {'ACTIVE': true},
            }
        },
        userCur: {
            method: 'user.current',
            params: {}
        },
        invoicesP: {
            method: 'crm.invoice.list',
            params: {
                filter: {
                    ">=PAY_VOUCHER_DATE": startDateInvoice,
                    "<=PAY_VOUCHER_DATE": endDateInvoice,
                    "STATUS_ID": "P",
                    '!IS_RECURRING': "Y"
                },
                order: {
                    //"RESPONSIBLE_ID": "ASC",
                    //"ID": "ASC"
                },
                select: ["ID", "STATUS_ID", "RESPONSIBLE_ID", "RESPONSIBLE_NAME", "RESPONSIBLE_LAST_NAME", "DATE_PAY_BEFORE", "PAY_VOUCHER_DATE", "PRICE", "ORDER_TOPIC"]
            }
        },
        invoicesA: {
            method: 'crm.invoice.list',
            params: {
                filter: {
                    //">=DATE_PAY_BEFORE": startDateInvoice,
                    // "<=DATE_PAY_BEFORE": endDateInvoice,
                    "!STATUS_ID": declineStatuses,
                    '!IS_RECURRING': "Y"
                },
                order: {
                    //"RESPONSIBLE_ID": "ASC",
                    //"ID": "ASC"
                },
                select: ["ID", "STATUS_ID", "RESPONSIBLE_ID", "RESPONSIBLE_NAME", "RESPONSIBLE_LAST_NAME", "DATE_PAY_BEFORE", "PAY_VOUCHER_DATE", "PRICE", "ORDER_TOPIC"]
            }
        },
    };

    for (var index in arCommands){
        arAllData[index] = [];
    }

    arAllData.invoices = {
        allInvoicesA: [],
        allInvoicesP: [],
        allInvoices: [],
    }

    call.batch(
        arCommands,
        function (res) {
            // console.log(res);

            for (var key in res){
                if (key === 'userCur') {
                    currentUserID = res[key].data.ID;
                    currentUserName = res[key].data.NAME + " " + res[key].data.LAST_NAME;
                    arAllData[key].push(res[key].data);
                } else if (key === 'entityDuedates') {
                    for(var index in res[key].data) {
                        let duedate = res[key].data[index];

                        if (!$.isEmptyObject(duedate)) {
                            if (!(duedate.NAME in arAllData[key])) {
                                arAllData[key][duedate.NAME] = [];
                            }

                            arAllData[key][duedate.NAME].push({
                                user: duedate.PROPERTY_VALUES.user,
                                date: duedate.PROPERTY_VALUES.date,
                                duedateAfter: duedate.PROPERTY_VALUES.duedateAfter,
                                duedateBefore: duedate.PROPERTY_VALUES.duedateBefore,
                                comment: duedate.PROPERTY_VALUES.comment
                            });
                        }
                    }
                } else if (key === 'invoicesP') {
                    for(var index in res[key].data){
                        let invoice = res[key].data[index];

                        arAllData.invoices.allInvoicesP.push(invoice);
                        arAllData.invoices.allInvoices.push(invoice);
                    }
                } else if (key === 'invoicesA') {
                    for(var index in res[key].data){
                        let invoice = res[key].data[index];

                        arAllData.invoices.allInvoicesA.push(invoice);
                        arAllData.invoices.allInvoices.push(invoice);
                    }
                } else {
                    for(var index in res[key].data){
                        arAllData[key].push(res[key].data[index]);
                    }
                }
            }

            //console.log(arAllData);

            $.each(arAllData.user, function (index, value) {
                allManagers[value.ID] = value.NAME + " " + value.LAST_NAME;
            })

            app.addProgressBar();
            app.getAllEndSums(startDateInvoice, endDateInvoice, mondayDate, sundayDate, '#deal-list', '#deal-sums');
            app.showSumByMonths();
        },
        function (error) {
            console.log(error);
        }
    );
};

// подсчет сумм по неделям ++++++++++++++++++++++++++++
application.prototype.getAllEndSums = function (startDate, endDate, mondayDate, sundayDate, listId, sumsId) {
    var arEntities = {}, arEntitiesIds = {}, sumUnpaid = 0;
    var curentDate = new Date(todayDate);
    curentDate = tempus(curentDate).format('%Y.%m.%d');
    //console.log(arAllData.entityCorrections);

    $.each(arAllData.entityCorrections,function(index, value){
        arEntities[value.NAME] = JSON.parse(value.PREVIEW_TEXT)['endSum'];
        arCorrections[value.NAME] = JSON.parse(value.PREVIEW_TEXT)['corrSum'];
        arCorrectionsIds[value.NAME] = value.ID;
        arEntitiesIds[value.NAME] = value.ID;

        /*if (value.DETAIL_TEXT !== null) {
            console.log("ID: "+value.ID);
            console.log("DATE: "+value.NAME);
            console.log(JSON.parse(value.DETAIL_TEXT));
            console.log('----------------------');
        }*/

        var curWeekDay = new Date(value.NAME);

        // если arCorrections[value.NAME] не объект, то создаем объект с датами
        if (arCorrections[value.NAME] == 0 || arCorrections[value.NAME] === undefined) {
            arCorrections[value.NAME] = {};

            curWeekDay.setDate(curWeekDay.getDate() - 7);

            for (var i=0;i<7;i++) {
                curWeekDay.setDate(curWeekDay.getDate() + 1);
                arCorrections[value.NAME][tempus(curWeekDay).format('%Y.%m.%d')] = ''
            }
        }
    });

    var prevEndDate = new Date(startDate);
    var prevEndSum = 0;

    if (arEntities.hasOwnProperty(tempus(prevEndDate).format('%Y.%m.%d'))) {
        prevEndSum = arEntities[tempus(prevEndDate).format('%Y.%m.%d')];
    }

    while (prevEndDate < endDate) {
        var curUpdateArr = [];
        var curEndDate = new Date(prevEndDate);
        var curStartDate = new Date(prevEndDate);
        var curWeekSum = 0;
        tempus(curStartDate).calc({day: -7});
        var prevWeekEndSum = arEndSums[tempus(curStartDate).format("%Y.%m.%d")];
        var curMondayDate = new Date(curStartDate);
        curMondayDate = tempus(curMondayDate).calc({day: 1}).format('%Y.%m.%d');

        if (prevWeekEndSum !== undefined) {
            curWeekSum = prevWeekEndSum;
        }

        curEndDate = tempus(curEndDate).format('%Y.%m.%d');

        $.each(arCorrections[curEndDate],function(corDate, corValue) {
            if (corValue !== '') {
                curWeekSum = corValue;
            }

            // плюсуем к сумме все счета за обрабатываемый день
            $.each(arAllData.invoices.allInvoices,function(index, value){
                var closeDate, priceInvoice;

                priceInvoice = parseInt(value.PRICE);

                if (value.STATUS_ID == 'P') {
                    closeDate = new Date(value.PAY_VOUCHER_DATE);
                } else {
                    closeDate = new Date(value.DATE_PAY_BEFORE);
                }

                closeDate = tempus(closeDate).format('%Y.%m.%d');

                // если счет не оплачен и просрочен (дата оплаты < сегодня), то плюсуем к сегодняшней сумме)
                if (value.STATUS_ID != 'P' && corDate == curentDate && closeDate <= curentDate) {
                    curWeekSum += priceInvoice;
                }

                // если счет не оплачен и дата оплаты позже сегодняшнего дня (и обрабатываемый день - не сегодня)
                /*if (value.STATUS_ID != 'P' && corDate != curentDate && closeDate > curentDate) {
                    curWeekSum += priceInvoice;
                }*/

                // если счет оплачен и дата оплаты == обрабатываемый день
                if (value.STATUS_ID == 'P' && (closeDate == corDate)) {
                    curWeekSum += priceInvoice;
                }

            });
        });

        var previewText = JSON.stringify({corrSum: arCorrections[curEndDate], endSum: curWeekSum});

        if (arEntities.hasOwnProperty(curEndDate)) {
            curUpdateArr = [
                'entity.item.update',
                {
                    ENTITY: 'corrections',
                    ID: arEntitiesIds[curEndDate],
                    PREVIEW_TEXT: previewText
                }
            ];
        } else {
            curUpdateArr = [
                'entity.item.add',
                {
                    ENTITY: 'corrections',
                    NAME: curEndDate,
                    PREVIEW_TEXT: previewText
                }
            ];
        }

        arEndSumsUpdate.push(curUpdateArr);
        arEndSums[curEndDate] = curWeekSum;

        tempus(prevEndDate).calc({day: 7});
    }

    var arCommands = [];
    arCommands = arEndSumsUpdate.splice(0, 50);

    var batchCallback = function () {
        arCommands = arEndSumsUpdate.splice(0, 50);
        var empty = true;

        if (!$.isEmptyObject(arCommands)) {
            empty = false;
        }
        if (!empty) {
            setTimeout(BX24.callBatch(arCommands, batchCallback), 500);
        }
    };
    setTimeout(BX24.callBatch(arCommands, batchCallback), 500);

    app.findAllInvoices(mondayDate, sundayDate, listId, sumsId);
};

// подсчет сумм по месяцам +++++++++++++++++++++++++++
application.prototype.showSumByMonths = function () {
    var self = this;
    var d = new Date();
    var prevMonthStart = new Date(d.getFullYear(), d.getMonth() - 1, 1);
    var prevMonthFinish = curMonthStart = new Date(d.getFullYear(), d.getMonth(), 1);
    var curMonthFinish = nextMonthStart = new Date(d.getFullYear(), d.getMonth() + 1, 1);
    var nextMonthFinish = new Date(d.getFullYear(), d.getMonth() + 2, 1);

    var prevMonthName = monthsRusNames[prevMonthStart.getMonth()];
    var curMonthName = monthsRusNames[curMonthStart.getMonth()];
    var nextMonthName = monthsRusNames[nextMonthStart.getMonth()];

    var sumPrevP = 0,
        sumCurP = 0,
        sumCurA = 0,
        sumNextA = 0;

    $.each(arAllData.invoices.allInvoicesP, function (index, invoice) {
        var payDateString1 = new Date(Date.parse(invoice.PAY_VOUCHER_DATE));

        if (payDateString1 >= prevMonthStart && payDateString1 <= prevMonthFinish) {
            sumPrevP += parseInt(invoice.PRICE);
        }

        if (payDateString1 >= curMonthStart && payDateString1 <= curMonthFinish) {
            sumCurP += parseInt(invoice.PRICE);
        }
    });

    $.each(arAllData.invoices.allInvoicesA, function (index, invoice) {
        var payDateString2 = new Date(Date.parse(invoice.DATE_PAY_BEFORE));

        if (payDateString2 >= curMonthStart && payDateString2 <= curMonthFinish) {
            sumCurA += parseInt(invoice.PRICE);
        }

        if (invoice.DATE_PAY_BEFORE == false) {
            sumCurA += parseInt(invoice.PRICE);
        }

        if (payDateString2 >= nextMonthStart && payDateString2 <= nextMonthFinish) {
            sumNextA += parseInt(invoice.PRICE);
        }
    });

    if(!self.active) {
        $('.lock').show();
		$('#sums-month-prev-1').html('<span class="sum-payed">-</span>');
		$('#sums-month-payed').html('<span class="sum-payed">-</span> + <span class="sum-not-payed">-</span>');
		$('#sums-month-next-1').html('<span class="sum-not-payed">-</span>');

		$('#dates-month-prev-1').html(prevMonthName);
		$('#date-month-from').html('<span class="todayWeek"><b>' + curMonthName + '</b></span>');
		$('#dates-month-next-1').html(nextMonthName);

    } else {
        $('.lock').hide();
		$('#sums-month-prev-1').html('<span class="sum-payed">' + accounting.formatNumber(sumPrevP, 0, " ") + '</span>');
		$('#sums-month-payed').html('<span class="sum-payed">' + accounting.formatNumber(sumCurP, 0, " ") + '</span> + <span class="sum-not-payed">' + accounting.formatNumber(sumCurA, 0, " ") + '</span>');
		$('#sums-month-next-1').html('<span class="sum-not-payed">' + accounting.formatNumber(sumNextA, 0, " ") + '</span>');

		$('#dates-month-prev-1').html(prevMonthName);
		$('#date-month-from').html('<span class="todayWeek"><b>' + curMonthName + '</b></span>');
		$('#dates-month-next-1').html(nextMonthName);
	}
};

// Поиск менеджеров, их счетов и оплат на обрабатываемой неделе ++++++++++++++
application.prototype.findAllInvoices = function(mondayDate, sundayDate, selectorPays, selectorSums) {
    var allInvoices = [], allInvoicesA = [], allInvoicesP = [], dayPays = [],
        dayPaysSum = 0, price = 0, payStatus = '', i = 0,
        payDate = new Date(), today2Date = new Date(), dateCur = new Date();

    // находим неоплаченные счета для обрабатываемой недели
    $.each(arAllData.invoices.allInvoicesA, function(index, value){
        var datePayBefore = false;
        if (value.DATE_PAY_BEFORE) {
            var datePayBefore = new Date(value.DATE_PAY_BEFORE);
        } else {
            datePayBefore = new Date();
        }
        if (datePayBefore < sundayDate) {
            allInvoicesA.push(value);
        }
    });

    for (let indexDeal in allInvoicesA) {
        dayPays = []; // массив для хранения платежей по дням
        dayPaysSum = 0; //  сумма за неделю

        payDate = new Date(allInvoicesA[indexDeal].DATE_PAY_BEFORE); // дата оплаты
        payDate.setHours(23); // устанавливаем время оплаты на 23 часа
        payDate.setMinutes(59);// устанавливаем время оплаты на 59 минут
        payDate.setSeconds(59);// устанавливаем время оплаты на 59 секунд

        today2Date = new Date(todayDate);
        today2Date.setHours(23); // устанавливаем время оплаты на 23 часа
        today2Date.setMinutes(59);// устанавливаем время оплаты на 59 минут
        today2Date.setSeconds(59);// устанавливаем время оплаты на 59 секунд
        price = parseInt(allInvoicesA[indexDeal]['PRICE']);

        dateCur = new Date(mondayDate);
        dateCur.setHours(23); // устанавливаем время оплаты на 23 часа
        dateCur.setMinutes(59);// устанавливаем время оплаты на 59 минут
        dateCur.setSeconds(59);// устанавливаем время оплаты на 59 секунд

        payStatus = 'pre-payed';

        // перебираем дни
        for (i=0;i<7;i++) {

            // если сейчас текущая неделя
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()) {

                // если день оплаты в обрабатываемый день
                if (dateCur.getDay() == payDate.getDay() && dateCur.getDate() == payDate.getDate() && dateCur.getFullYear() == payDate.getFullYear()) {
                    if (payDate < todayDate) {
                        dayPays[i] = 0;
                        dayPays[(todayWeekDay - 1)] = price;
                        payStatus = 'not-payed';
                    } else {
                        dayPays[i] = price;
                    }

                    dayPaysSum = price;
                } else {

                    if (dayPays[i] == undefined) dayPays[i] = 0;

                    if (i == 6) {
                        if (payDate.getDay() == today2Date.getDay() && payDate.getDate() == today2Date.getDate() && payDate.getFullYear() == today2Date.getFullYear()) {

                        } else if (payDate >= today2Date && payDate <= sundayDate && dayPaysSum != 0) {
                            dayPays[todayWeekDay-1] = 0;
                        } else {
                            dayPays[todayWeekDay-1] = price;
                        }
                    }

                }

                // если у счета на этой неделе нет оплат (оплата просрочена или оплата в будущем)
                if (dayPaysSum == 0) {
                    if (i == 6) dayPaysSum = price;

                    if (todayWeekDay == 0) {
                        dayPays[6] = price;
                    } else {
                        //dayPays[(todayWeekDay-1)] = price;
                    }

                    payStatus = 'not-payed';
                }

                if (payDate > todayDate && payDate > mondayDate && payDate != '') payStatus = 'pre-payed';

            } else { // для будущей недели

                // если день оплаты в обрабатываемый день
                if (dateCur.getDay() == payDate.getDay() && dateCur.getDate() == payDate.getDate() && dateCur.getFullYear() == payDate.getFullYear()) {
                    if (payDate < mondayDate || payDate == '') {
                        dayPays[i] = 0;
                        dayPays[0] = price;
                        payStatus = 'not-payed';
                    } else {
                        dayPays[i] = price;
                    }
                    dayPaysSum = price;

                } else {
                    if (dayPays[i] == undefined) dayPays[i] = 0;
                }

                // если у счета на этой неделе нет оплат (оплата просрочена или оплата в будущем)
                if ( (dayPaysSum == 0 && payDate == 'Invalid Date') || (dayPaysSum == 0 && payDate < mondayDate) ) {
                    dayPaysSum = price;
                    dayPays[0] = dayPaysSum;
                    payStatus = 'not-payed';
                }

            }

            dateCur.setDate(dateCur.getDate()+1); // следующий день
        }

        // в конце добавляем всю информацию в массив allInvoices

        // для текущей недели
        if (mondayDate.getTime() == mondayDateCur.getTime()) {
            allInvoices.push([
                allInvoicesA[indexDeal]['RESPONSIBLE_NAME']+" "+allInvoicesA[indexDeal]['RESPONSIBLE_LAST_NAME'],
                allInvoicesA[indexDeal]['RESPONSIBLE_ID'],
                [
                    allInvoicesA[indexDeal]['ORDER_TOPIC'],
                    "/crm/invoice/show/"+allInvoicesA[indexDeal]['ID']+"/",
                    payStatus,
                    dayPaysSum,
                    dayPays
                ],
                allInvoicesA[indexDeal]['DATE_PAY_BEFORE']
            ]);
        }

        // для будущей недели
        if (mondayDate > mondayDateCur && payDate >= mondayDate && dayPaysSum != 0) {
            allInvoices.push(
                [
                    allInvoicesA[indexDeal]['RESPONSIBLE_NAME']+" "+allInvoicesA[indexDeal]['RESPONSIBLE_LAST_NAME'],
                    allInvoicesA[indexDeal]['RESPONSIBLE_ID'],
                    [
                        allInvoicesA[indexDeal]['ORDER_TOPIC'],
                        "/crm/invoice/show/"+allInvoicesA[indexDeal]['ID']+"/",
                        payStatus,
                        dayPaysSum,
                        dayPays
                    ],
                    allInvoicesA[indexDeal]['DATE_PAY_BEFORE']
                ]);
        }
    }

    $.each(arAllData.invoices.allInvoicesP, function(index, value){
        var payDate = new Date(value.PAY_VOUCHER_DATE);
        if (payDate > mondayDate && payDate < sundayDate) {
            allInvoicesP.push(value);
        }
    });

    for (let indexDeal in allInvoicesP) {
        dayPays = []; // массив для хранения платежей по дням
        dayPaysSum = 0; // массив для хранения суммы за неделю
        payDate = new Date(allInvoicesP[indexDeal].PAY_VOUCHER_DATE);
        price = parseInt(allInvoicesP[indexDeal]['PRICE']);
        dateCur = new Date(mondayDate);

        for (i=0;i<7;i++) {
            if (dateCur.getDay() == payDate.getDay() && dateCur.getDate() == payDate.getDate() && dateCur.getFullYear() == payDate.getFullYear()) {
                dayPays.push(price);
                dayPaysSum = price;
            } else {
                dayPays.push(0);
            }
            dateCur.setDate(dateCur.getDate()+1); // следующий день
        }

        // в конце добавляем всю информацию в массив allInvoices
        allInvoices.push([
            allInvoicesP[indexDeal]['RESPONSIBLE_NAME']+" "+allInvoicesP[indexDeal]['RESPONSIBLE_LAST_NAME'],
            allInvoicesP[indexDeal]['RESPONSIBLE_ID'],
            [
                allInvoicesP[indexDeal]['ORDER_TOPIC'],
                "/crm/invoice/show/"+allInvoicesP[indexDeal]['ID']+"/",
                "payed",
                dayPaysSum,
                dayPays
            ]
        ]);
    }

    allInvoices.sort();

    // логика разделения счетов на доходы и расходы
    var arIncomes = [], arExpenses = [];
    for (indexDeal in allInvoices) {
        if (allInvoices[indexDeal][2][3] > 0) {
            arIncomes.push(allInvoices[indexDeal]);
        } else if (allInvoices[indexDeal][2][3] < 0) {
            arExpenses.push(allInvoices[indexDeal]);
        }
    }

    if (arIncomes.length > 0) {
        $(selectorPays + ' #deal-income').show();
    }

    if (arExpenses.length > 0) {
        $(selectorPays + ' #deal-expense').show();
    }

    app.addProgressBar();

    displayPays(arIncomes, selectorPays + ' #deal-income', selectorSums); // вывод менеджеров и счетов в таблицу
    displayPays(arExpenses, selectorPays + ' #deal-expense', selectorSums); // вывод менеджеров и счетов в таблицу

    displaySums(arIncomes, arExpenses, "#deal-income", "Доходы", 'incomes'); // вывод верхней строки сумм в таблицу

    //displayPays(allInvoices, selectorPays, selectorSums); // вывод менеджеров и счетов в таблицу
};

// Вывод менеджеров, их счетов и оплат, а так же сумм менеджеров
function displayPays(allInvoices, selector, selectorSums) {
    var self = this;
    var paysHTML = '';
    var managerPaySums = [];
    var managerNotPaySums = [];
    var managerSums = [];
    var manInc = 0;
    managersCount = 0;
    invoicesCount = 0;

    allInvoices.sort(function (a, b) {
        if (a[0] > b[0]) {
            return 1;
        } else if (a[0] < b[0]) {
            return -1;
        } else if (a[0] == b[0]) {
            if (a[5] > b[5]) {
                return 1;
            } else if (a[5] < b[5]) {
                return -1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    });

    for (var i=0;i<allInvoices.length;i++) {
        var invoicePathArr = allInvoices[i][2][1].split("/");
        var invoiceId = invoicePathArr[4];
        var invoiceDueDate = false;
        var invoiceDueDateReverse = false;
        var invoiceStatus = allInvoices[i][2][2];
        var tooltipHTML = '';

        // [start] tooltipHTML ///////////////////////////////////////////////////////////////////////////////////////////
        if (arAllData.entityDuedates[invoiceId] !== undefined && arAllData.entityDuedates[invoiceId] !== '') {
            tooltipHTML += '<div>';
            var countDues = 0;
            arEntityDuedates = arAllData.entityDuedates[invoiceId];
            dpDuedateAfter = arEntityDuedates[arEntityDuedates.length - 1].duedateAfter.split('.');
            invoicesIds[invoiceId] = new Date(dpDuedateAfter[1] + '.' + dpDuedateAfter[0] + '.' + dpDuedateAfter[2]);
            $.each(arAllData.entityDuedates[invoiceId], function(i, objDuedate) {
                console.log(objDuedate);
                if (countDues > 0) {
                    tooltipHTML += '<br/><br/>';
                }
                tooltipHTML += '<div class="text-content" style="padding-bottom: 20px;">';
                tooltipHTML += (invoiceStatus != 'payed') ? '<a class="report-date" href="#">Перенести на дату</a>' :'';
                tooltipHTML += '<span class="text-center2"><span class="first-line">'+ objDuedate.user +'</span> <span class="second-line">'+ objDuedate.duedateBefore +'<span class="blue-arrow"> &rarr; </span>'+ objDuedate.duedateAfter +' '+ objDuedate.date +'</span><br/>';
                tooltipHTML += '<span class="text-center3"> → </span>';
                if (objDuedate.comment != '') {
                    tooltipHTML += '<br/><span class="text-center">'+ objDuedate.comment +'</span>';
                }

                countDues++;
            });
            tooltipHTML += '</div>';
        } else {
            tooltipHTML += '<div class="text-content">';
            tooltipHTML += (invoiceStatus != 'payed') ? '<a class="report-date" href="#">Перенести на дату</a>' :'';
            tooltipHTML += '<span class="text-center4">Нет изменений срока оплаты</span></div>';
        }

        // [end] tooltipHTML ///////////////////////////////////////////////////////////////////////////////////////////

        if (allInvoices[i][3] !== undefined && allInvoices[i][3] !== '') {
            invoiceDueDate = tempus(allInvoices[i][3]).format('%d.%m.%Y');
            invoiceDueDateReverse = tempus(allInvoices[i][3]).format('%Y-%m-%d');
        } else { //если нет даты оплаты то ставим сегодня
            invoiceDueDate = tempus().calc({day: 0}).format('%d.%m.%Y');
            invoiceDueDateReverse = tempus().calc({day: 0}).format('%Y-%m-%d');
        }

        if (i == 0) {
            // для первой строчки (первого менеджера и первого счета)
            // вывод первого менеджера
            //paysHTML = "<tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td id='managpay"+ manInc +"'></td><td id='managprepay"+ manInc +"'></td><td id='managsumpay"+ manInc +"'></td><td class='td-hr'></td><td colspan='7'></td></tr>";
            paysHTML = "<tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td class='managpay' id='managpay"+ manInc +"'><div class='managpay number'></div></td><td>+</td><td class='managprepay' id='managprepay"+ manInc +"'><div class='managprepay number'></div></td><td class='td-hr'></td><td colspan='7'></td></tr>";

            // вывод первой оплаты первого менеджера
            //paysHTML += "<tr id='invoice"+ invoicesCount +"'><td class='left' colspan='4'><a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>"
            paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='4'>";
            paysHTML += "<a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";

            // перебор по дням
            var curDate = new Date(mondayDate);
            for (var j=0;j<7;j++) {
                if (allInvoices[i][2][4][j] == 0 || allInvoices[i][2][4][j] == null) { // если в этот день нет оплаты, то пустое поле
                    paysHTML += `<td num-day="${j+1}" date-day="${curDate.getDate()+j}"></td>`;
                } else { // иначе записываем в поле сумму счета и
                    paysHTML += "<td class='" + allInvoices[i][2][2] + "' data-invoiceId='"+ invoiceId +"'";
                    if (tooltipHTML !== '') {
                        paysHTML += " data-duedates='true'";
                    }
                    if (invoiceDueDate) {
                        paysHTML += "><span><a class='edit-duedates click-black' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "<a/></span></td>";
                        createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
                    } else {
                        paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                    }
                }
            }

            // подсчет сумм оплат по менеджеру для первого счета
            if (allInvoices[i][2][2] == 'payed') {
                managerPaySums[manInc] = allInvoices[i][2][3];
                managerNotPaySums[manInc] = 0;
            } else {
                managerNotPaySums[manInc] = allInvoices[i][2][3];
                managerPaySums[manInc] = 0;
            }
            managerSums[manInc] = allInvoices[i][2][3];

            paysHTML += "</tr>";

            managersCount++; // переход на следующего менеджера
            invoicesCount++; // переход на следующий счет

        } else {
            // если предыдущий счет от другого менеджара, то выводим нового менеджера
            if (allInvoices[i][1] != allInvoices[i-1][1]) {
                manInc++;
                managerPaySums[manInc] = 0;
                managerNotPaySums[manInc] = 0;
                managerSums[manInc] = 0;

                // вывод менеджера
                //paysHTML += "</tr><tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td id='managpay"+ manInc +"'></td><td id='managprepay"+ manInc +"'></td><td id='managsumpay"+ manInc +"'></td><td class='td-hr'></td><td colspan='7'></td></tr>";
                paysHTML += "</tr><tr class='manager' id='manager"+ managersCount +"'><td class='left manager-name'>"+ allInvoices[i][0] +"</td><td class='managpay' id='managpay"+ manInc +"'><div class='managpay number'></div></td><td>+</td><td class='managprepay' id='managprepay"+ manInc +"'><div class='managprepay number'></div></td><td class='td-hr'></td><td colspan='7'></td></tr>";
                managersCount++; // переход на следующего менеджера
            }

            // вывод названия счетов и их оплат
            //paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='3'><a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";
            paysHTML += "<tr class='invoice-tr' id='invoice"+ invoicesCount +"'><td class='left' colspan='4'>";
            /*if (invoiceDueDate) {
                paysHTML += "<button class='edit-duedates' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>✎</button> ";
                createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
            }*/
            paysHTML += "<a target='blank' href='https://"+ curDomain + allInvoices[i][2][1] +"'>"+ allInvoices[i][2][0] +"</a></td><td class='td-hr'></td>";
            invoicesCount++; // переход к следующему счету

            // перебор по дням
            let curDate = new Date(mondayDate);
            for (var j=0;j<7;j++) {
                if (allInvoices[i][2][4][j] == 0 || allInvoices[i][2][4][j] == null) { // если в этот день нет оплаты, то пустое поле
                    paysHTML += `<td num-day="${j+1}" date-day="${curDate.getDate()+j}"></td>`;
                } else { // иначе записываем в поле сумму счета и
                    paysHTML += "<td class='" + allInvoices[i][2][2] + "' data-invoiceId='"+ invoiceId +"'";
                    if (tooltipHTML !== '') {
                        paysHTML += " data-duedates='true'";
                    }

                    if (invoiceDueDate) {
                        paysHTML += "><span><a class='edit-duedates click-black' data-invoiceId='"+ invoiceId +"' data-curDuedate='"+ invoiceDueDate +"'>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "<a/></span></td>";
                        createDuedateModal(invoiceId, invoiceDueDate, invoiceDueDateReverse);
                    } else {
                        paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                    }
                    //paysHTML += "><span>" + accounting.formatNumber(allInvoices[i][2][4][j], 0, " ") + "</span></td>";
                }
            }

            // подсчет сумм по менеджерам
            if (allInvoices[i][2][2] == 'payed') {
                managerPaySums[manInc] += allInvoices[i][2][3];
            } else {
                managerNotPaySums[manInc] += allInvoices[i][2][3];
            }
            managerSums[manInc] += allInvoices[i][2][3];

            paysHTML += "</tr>";
        }
    }

    // Выделение столбца таблицы текущей даты
    $(`td[date-day="${new Date().getDate()}"]`).css('background-color','rgba(24, 91, 207, 0.1)');
    //---------------------------------------

    $(selector).after(paysHTML);

    // вывод сумм оплаченных счетов менеджера
    for (i=0;i<managerPaySums.length;i++) {
        var man_class = "#managpay"+i;
        $(man_class).find('.number').html(accounting.formatNumber(managerPaySums[i], 0, " "));
    }

    // вывод сумм неоплаченных счетов менеджера
    for (i=0;i<managerNotPaySums.length;i++) {
        var man_class = "#managprepay"+i;
        $(man_class).find('.number').html(accounting.formatNumber(managerNotPaySums[i], 0, " "));
    }
    BX24.fitWindow();
}

// Вывод строки сумм
function displaySums(allInvoices, arExpenses, selectorSums, title, step) {
        console.log(allInvoices);
    var sumDays = [0, 0, 0, 0, 0, 0, 0];

    for (var i=0;i<allInvoices.length;i++) {
        if (allInvoices[i][2][2] == 'payed') {
            sumPayed += allInvoices[i][2][3];
            thisWeekSumObj[step].sumPayed += allInvoices[i][2][3];
        } else {
            sumNotPayed += allInvoices[i][2][3];
            thisWeekSumObj[step].sumNotPayed += allInvoices[i][2][3];
        }

        for (var j=0;j<7;j++) {
            sumDays[j] += allInvoices[i][2][4][j];
        }
    }

    var sumsHTML = "<td class='first-col'>" + title + "</td><td class='sum'></td><td class='sum'></td><td class='td-hr'></td>";

    for (i=0;i<7;i++) {
        if (sumDays[i] != 0) {
            sumsHTML += "<td class='td-days'>"+ accounting.formatNumber(sumDays[i], 0, " ") +"</td>";
        } else {
            sumsHTML += "<td class='td-days'>0</td>";
        }
    }

    $(sumsHTML).appendTo(selectorSums);

    if (sumFlag === false) {
        sumFlag = true;
        displaySums(arExpenses, arExpenses, "#deal-expense", "Расходы", 'expenses'); // вывод верхней строки сумм в таблицу
    } else {
        // вывод верхней строки сумм в таблицу
        $('#deal-income .td-days').each(function (i) {
            var dayIncomesSum = $(this).text().split(' ').join('');
            var dayExpensesSum = $('#deal-expense .td-days:eq('+ i +')').text().split(' ').join('') || 0;
            var daySum = parseInt(dayIncomesSum) + parseInt(dayExpensesSum);

            fullSumDays[i] = daySum;

            $('#deal-sums .td-days:eq('+ i +')').html(accounting.formatNumber(daySum, 0, " "));
        });

        // показ суммы за текущую неделю
        var sumPayed = parseInt(thisWeekSumObj['incomes'].sumPayed) + parseInt(thisWeekSumObj['expenses'].sumPayed);
        var sumNotPayed = parseInt(thisWeekSumObj['incomes'].sumNotPayed) + parseInt(thisWeekSumObj['expenses'].sumNotPayed);

        if (sumPayed == 0) {
            $('#sums-payed').html("<span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()) {
                thisWeekSum = "<span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>";
            }
        } else if (sumNotPayed == 0) {
            $('#sums-payed').html("<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()) {
                thisWeekSum = "<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span>";
            }
        } else {
            $('#sums-payed').html("<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span> + <span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>");
            if (mondayDate.getDate() == mondayDateCur.getDate() && mondayDate.getMonth() == mondayDateCur.getMonth() && mondayDate.getFullYear() == mondayDateCur.getFullYear()){
                thisWeekSum = "<span class='sum-payed'>"+accounting.formatNumber(sumPayed, 0, " ")+"</span> + <span class='sum-not-payed'>"+accounting.formatNumber(sumNotPayed, 0, " ")+"</span>"
            }
        }

        loadCorrections(mondayDate, allInvoices);
    }

    BX24.fitWindow();
}

/* Корректировки */
function loadCorrections(startDate, pay) {
    var arDates = [tempus(startDate).format('%Y.%m.%d')];
    var modDate = new Date(startDate);

    for (var i=1;i<7;i++) {
        modDate.setDate(modDate.getDate() + 1);
        arDates.push(tempus(modDate).format('%Y.%m.%d'));
    }

    var arCorrections = {};
    var beginHTML = '<td class="text-center" colspan="4">Остаток на начало дня</td><td class="td-hr"></td>';
    // var corrHTML = '<td class="text-center" colspan="4">Скорректированный остаток на начало дня</td><td class="td-hr"></td>';
    var corrHTML = '<td class="text-center" colspan="4">Остаток на начало дня</td><td class="td-hr"></td>';
    var endHTML = '<td class="text-center" colspan="4">Итого</td><td class="td-hr"></td>';
    var modalHTML = "";
    var tooltipHTML = [];
    var sumCorrEnd = 0;
    var prevEndSum = 0;
    var prevEndDate = new Date(mondayDate);
    var calcDate = 1;
    var arCommands = [];
    var curWeekSundayDate = new Date(prevEndDate);

    curWeekSundayDate = tempus(curWeekSundayDate).calc({day: 6}).format('%Y.%m.%d');

    prevEndDate = tempus(prevEndDate).calc({day: -calcDate}).format('%Y.%m.%d');

    $.each(arAllData.entityCorrections, function(index, value){
        if (~arDates.indexOf(value.NAME)) {
            arFullCorrections[value['NAME']] = JSON.parse(value.PREVIEW_TEXT);
            arCorrections[value['NAME']] = arFullCorrections[value['NAME']]['corrSum'];
            arCorrectionsIds[value['NAME']] = value['ID'];
        }

        var objHistory = JSON.parse(value.DETAIL_TEXT);

        if (objHistory != null) {
            // TODO: сделать перебор по объекту objHistory (дни недели) и сгенерировать для каждого дня массив историй
            $.each(objHistory, function (historyDate, arHistory) {
                if (arHistories[value['NAME']] === undefined) {
                    arHistories[value['NAME']] = {};
                }

                if (arHistories[value['NAME']][historyDate] === undefined) {
                    arHistories[value['NAME']][historyDate] = {};
                }

                /*if (arHistories[value['NAME']][datetime] === undefined) {
                    arHistories[value['NAME']][datetime] = {};
                }*/

                $.each(arHistory, function (datetime, history) {
                    arHistories[value['NAME']][historyDate][datetime] = {
                        username: history['username'],
                        sum_before: history['sum_before'],
                        sum_after: history['sum_after'],
                    };
                });
            })
        }

        if (value.NAME == prevEndDate) {
            prevEndSum = JSON.parse(value.PREVIEW_TEXT)['endSum'];
            /*console.log(value);
            console.log('prevEndDate: '+prevEndDate);
            console.log('prevEndSum: '+prevEndSum);*/
        }

        /*if (value.ID == '15689') {
            console.log(arCorrections[value.NAME]);
            console.log(arHistories[value.NAME]);
            console.log('-----------------------');
        }*/
    });

    $.each(arDates,function(index, value){
        //console.log(value+ ': ' +prevEndSum);

        beginHTML += "<td>"+accounting.formatNumber(prevEndSum, 0, " ")+"</td>";

        var daySumCorr = 0;

        if (arCorrections[curWeekSundayDate] === undefined) {
            arCorrections[curWeekSundayDate] = {};
        }

        if (arCorrections[curWeekSundayDate][value] !== '') {
            daySumCorr = parseInt(arCorrections[curWeekSundayDate][value]);
        } else {
            daySumCorr = prevEndSum;
        }
        corrHTML += "<td><a data-toggle='modal' data-target='#corrModal-"+(index+1)+"'>"+accounting.formatNumber(daySumCorr, 0, " ")+"<a/></td>";

        // sumCorrEnd = parseInt(daySumCorr)+parseInt(fullSumDays[index]);
        sumCorrEnd = parseInt(fullSumDays[index]);
        prevEndSum = sumCorrEnd;

        /*if (index == arDates.length-1) {
            //updateEndSum(value, prevEndSum);
            if (prevEndSum < 0) {
                thisWeekSum = "<span class='sum-span corrEndMinus'>" + accounting.formatNumber(prevEndSum, 0, " ") + "</span>";
            } else {
                thisWeekSum = "<span class='sum-span corrEndPlus'>" + accounting.formatNumber(prevEndSum, 0, " ") + "</span>";
            }
            $('#sums-payed').html(thisWeekSum);
        }*/

        var corrEndClass = '';

        if (sumCorrEnd < 0) {
            corrEndClass = 'corrEndMinus';
        } else {
            corrEndClass = 'corrEndPlus';
        }

        //indicatorHTML += "<td class='expanded-corr expanded-corr"+(index+1)+" "+indicatorClass+"'></td>";
        endHTML += "<td class='"+corrEndClass+"'>"+accounting.formatNumber(sumCorrEnd, 0, " ")+"</td>";


        /*if (isAdmin) {
            modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
                "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
                "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
                "<h4 class='modal-title'>Корректировка остатков</h4></div>" +
                "<div class='modal-body text-center' data-date='"+value+"'>";
            if (arCorrections[value] && arCorrections[value][0]) {
                modalHTML += "<div class='text-center sumCorrBefore' data-sumCorrBefore='"+arCorrections[value][0]+"'>Текущая сумма "+arCorrections[value][0]+"</div>";
            }

            for(var i=1;i<8;i++) {
                var inputValue = '';
                if (arCorrections[value] && arCorrections[value][i]) {
                    inputValue = arCorrections[value][i];
                }
                modalHTML += "<label>"+corrStrokes[i-1]+"</label> <input class='text-center inputCorr inputCorr-"+i+"' type='number' value='"+inputValue+"' size='5'><br/>"
            }
            modalHTML += "</div><div class='modal-footer'>" +
                "<button type='button' class='btn btn-success saveCorr' data-dismiss='modal' data-date='"+value+"'>Сохранить</button>" +
                "<button type='button' class='btn' data-dismiss='modal'>Закрыть</button>" +
                "</div></div></div></div>";
        } else {
            modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
                "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
                "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
                "<h4 class='modal-title'>Корректировка остатков</h4></div>" +
                "<div class='modal-body text-center' data-date='"+value+"'>";
            modalHTML += "<div class='text-center'>Корректировки доступны только администраторам приложения.</div>";
            modalHTML += "</div><div class='modal-footer'>" +
                "<button type='button' class='btn' data-dismiss='modal'>Закрыть</button>" +
                "</div></div></div></div>";
        }*/


        modalHTML += "<div class='modal fade' id='corrModal-"+(index+1)+"' role='dialog'>" +
            "<div class='modal-dialog'><div class='modal-content'><div class='modal-header'>" +
            "<button type='button' class='close' data-dismiss='modal'>&times;</button>" +
            "<h4 class='modal-title'>Корректировка остатков на счетах</h4></div>" +
            "<div class='modal-body text-center' data-date='"+value+"'>";

        /*if (arCorrections[value] && arCorrections[value][0]) {
            modalHTML += "<div class='text-center sumCorrBefore' data-sumCorrBefore='"+daySumCorr+"'>Текущая сумма "+daySumCorr+"</div>";
        }*/

        modalHTML += "<label>Сумма</label><input class='text-center inputCorr' type='number' autofocus='autofocus' placeholder='"+accounting.formatNumber(daySumCorr, 0, " ")+"' size='5' data-sumCorrBefore='"+daySumCorr+"'><br/>"

        modalHTML += "</div><div class='modal-footer'>" +
            "<button type='button' class='btn btn-success saveCorr' data-dismiss='modal' data-date='"+value+"'>Перенести</button>" +
            "<button type='button' class='btn' data-dismiss='modal'><span class='line'>Отменить перенос</span></button>" +
            "</div></div></div></div>";


        if (arHistories[curWeekSundayDate] === undefined) {
            arHistories[curWeekSundayDate] = {};
        }

        if (arHistories[curWeekSundayDate][value] !== undefined) {
            var arCount = 0;
            tooltipHTML[index+1] = '';
            $.each(arHistories[curWeekSundayDate][value],function(datetime, value){
                if (arCount != 0) {

                    tooltipHTML[index+1] += "<br/>";
                }
                tooltipHTML[index+1] += "<div class='text-center'>"+value['username']+"<br/>("+datetime+")</div>";
                tooltipHTML[index+1] += "<div class='text-center'>"+value['sum_before']+" &#8594; "+value['sum_after']+"</div>";
                arCount++;
            })
        }
    });

    // $('#balance-begin').append(beginHTML);
    $('#balance-corr').append(corrHTML);
    $('#balance-end').append(endHTML);

    $('body').append(modalHTML);

    for (var j = 1; j < 8; j++) {
        $('td a[data-target="#corrModal-' + parseInt(j) + '"]').tooltip({
            title: tooltipHTML[j],
            html: true,
            placement: "left"
        });
    }

    if (BX24.appOption.get("restart") === 'Y') {
        BX24.appOption.set("restart", "N", function () {
            BX24.reloadWindow();
        });
    } else {
        app.addTooltipsToDuedates(pay);
    }
}

// Инициализация тултипов для комментариев о переносе срока оплаты++++++++++++
application.prototype.addTooltipsToDuedates = function (pay) {
	var linkHtml = '';
	var self = this;
	var classLock = '';


	if(!self.active) {
		linkHtml = '<i class="fas fa-lock"></i>';
		classLock = 'js-lock';
	}
    $('td[data-duedates="true"]').each(function () {
        var invoiceId = $(this).data("invoiceid");
        var tooltipHTML = '<div class="text-content">';
        var bInvoicePayed = false;
        for(var ind in arAllData.invoices.allInvoicesP) { if(arAllData.invoices.allInvoicesP[ind].ID == invoiceId) bInvoicePayed = true}
        tooltipHTML += (bInvoicePayed != true) ?'<a class="report-date '+classLock+'" href="#">'+linkHtml+' Перенести на дату</a>' :'';
        tooltipHTML += '<span class="text-center4">Нет изменений срока оплаты</span></div>';


        var countDues = 0;
        var comment = false;

        if (arAllData.entityDuedates[invoiceId] !== undefined && arAllData.entityDuedates[invoiceId] !== '') {
            tooltipHTML = '<div class="text-content" style="padding-bottom: 20px;">';
   		    tooltipHTML = '<div class="tip-content"><a class="report-date '+classLock+'" href="#">'+linkHtml+'Перенести на дату</a>';
            $.each(arAllData.entityDuedates[invoiceId], function(i, objDuedate) {
                // console.log(objDuedate);
                tooltipHTML += '<span class="text-center2"><span class="first-line">'+ objDuedate.date +' &bull; '+ objDuedate.user +'</span><span class="second-line">'+ objDuedate.duedateBefore +'<span class="blue-arrow">&rarr;</span>'+ objDuedate.duedateAfter +'</span>';
                if (objDuedate.comment != '') {
                    comment = true;
                    tooltipHTML += '<span class="text-center3">' + objDuedate.comment + '</span>';
                }
                tooltipHTML += '</span><hr>';
                countDues++;
            });
            tooltipHTML += '</div></div>';
        }

        var thisCell = $("td[data-invoiceid='"+ invoiceId +"'] span");

        if (comment) thisCell.addClass('nopay');

        thisCell.tooltip({
            trigger: 'click',
            title: tooltipHTML + invoiceItems[invoiceId],
            html: true,
            placement: function(tip, element) { //$this - экземпляр tooltip
                // tip - DOM-узел всплывающей подсказки
                // element - DOM-элемент, который вызвал эту подсказку
                // получаем JavaScript объект содержащий координаты элемента element
                var position = $(element).position();
                let wbody = $('#deal-list').width();

                /* если его left-координата меньше или равно 300px, то подсказку будем показывать снизу от элемента, иначе слева от элемента */

                if (position.left <= (wbody-150)) {
                    return "bottom";
                } else {
                    return "left";
                }
            },
        });

        thisCell.on('shown.bs.tooltip', function () {
            $('.duedate-input-'+invoiceId).datepicker({
                minDate: new Date(),
                inline: true,
                autoClose: true,
                navTitles: {
                    days: 'MM yyyy'
                },
                onSelect: function(formattedDate, date, inst) {
                    $('.duedate-textarea-'+invoiceId).focus();
                },
                onRenderCell: function(date, cellType) {
                    if (invoicesIds[invoiceId] && invoicesIds[invoiceId].toJSON() == date.toJSON()) {
                        return {
                            classes: 'day-report'
                        }
                    }
                }
            });
        });

        // $(document).not(thisCell).on('click', function() {
            // thisCell.tooltip('disable');
        // });

    });

    app.addProgressBar();
    app.findAllInvoicesPrev(prevArguments[prevCount]['prevDays'], mondayDate, sundayDate, prevArguments[prevCount]['dates'], prevArguments[prevCount]['sums']);
}

// Поиск счетов и оплат на прошлых и будущих неделях+++++++++++++++++++++++++
application.prototype.findAllInvoicesPrev = function(prevDays, mondayDateCurPrev, sundayDateCurPrev, selectorDays, selectorSums) {

    var sumPrev = 0;
    var mondayDatePrev = new Date(mondayDateCurPrev);
    var sundayDatePrev = new Date(sundayDateCurPrev);

    mondayDatePrev.setDate(mondayDateCurPrev.getDate()-prevDays);
    sundayDatePrev.setDate(sundayDateCurPrev.getDate()-prevDays);

    // вывод даты
    var dateFrom = mondayDatePrev.getDate();
    if (dateFrom < 10) {
        dateFrom = "0"+dateFrom;
    }
    var monthFrom = mondayDatePrev.getMonth()+1;
    if (monthFrom < 10) {
        monthFrom = "0"+monthFrom;
    }
    var dateUpto = sundayDatePrev.getDate();
    if (dateUpto < 10) {
        dateUpto = "0"+dateUpto;
    }
    var monthUpto = sundayDatePrev.getMonth();

    if (mondayDatePrev < mondayDateCur) {
        // счета со статусом "Оплачено" для прошлых недель
        $.each(arAllData.invoices.allInvoicesP, function (index, invoice) {
            var payDateString = new Date(Date.parse(invoice.PAY_VOUCHER_DATE));

            if (payDateString >= mondayDatePrev && payDateString <= sundayDatePrev) {
                sumPrev += parseInt(invoice.PRICE);
            }
        });



        $(selectorDays).html("<a href='#' class='click' onclick='app.changeWeek("+prevDays+")'>"+dateFrom+" - "+dateUpto+" "+months[monthUpto]+"</a>");

        // вывод суммы
        if (mondayDatePrev.getDate() == mondayDateCur.getDate() && mondayDatePrev.getMonth() == mondayDateCur.getMonth() && mondayDatePrev.getFullYear() == mondayDateCur.getFullYear()) {
            $(selectorSums).html(thisWeekSum);
        } else {
            $(selectorSums).html("<span class='sum-payed'>"+accounting.formatNumber(sumPrev, 0, " ")+"</span>");
        }

        if (todayDate > mondayDatePrev && todayDate < sundayDatePrev) {
            $(selectorDays).addClass('today');
            $(selectorDays).find('a').addClass('todayWeekA');
        } else {
            $(selectorDays).removeClass('today');
            $(selectorDays).find('a').removeClass('todayWeekA');
        }

    } else {
        // счета со статусами не "оплачено" и не "отклонен" для будущих недель
        $.each(arAllData.invoices.allInvoicesA, function (index, invoice) {
            var payDateString = new Date(Date.parse(invoice.DATE_PAY_BEFORE));

            if (payDateString >= mondayDatePrev && payDateString <= sundayDatePrev) {
                sumPrev += parseInt(invoice.PRICE);
            }
        });

        $(selectorDays).html("<a href='#' class='click' onclick='app.changeWeek("+prevDays+")'>"+dateFrom+" - "+dateUpto+" "+months[monthUpto]+"</a>");

        // вывод суммы
        if (mondayDatePrev.getDate() == mondayDateCur.getDate() && mondayDatePrev.getMonth() == mondayDateCur.getMonth() && mondayDatePrev.getFullYear() == mondayDateCur.getFullYear()) {
            $(selectorSums).html(thisWeekSum);
        } else {
            $(selectorSums).html("<span class='sum-not-payed'>"+accounting.formatNumber(sumPrev, 0, " ")+"</span>");
        }

        if (todayDate > mondayDatePrev && todayDate < sundayDatePrev) {
            $(selectorDays).addClass('today');
            $(selectorDays).find('a').addClass('todayWeekA');
        } else {
            $(selectorDays).removeClass('today');
            $(selectorDays).find('a').removeClass('todayWeekA');
        }
    }

    prevCount++;
    if (prevCount < 7) {
        app.findAllInvoicesPrev(prevArguments[prevCount]['prevDays'], mondayDateCurPrev, sundayDateCurPrev, prevArguments[prevCount]['dates'], prevArguments[prevCount]['sums']);
    } else {
        Pace.start();
        // app.toggleLoading();
        $('#week').fadeIn(300, 'swing', function () {
            BX24.fitWindow();
        });
    }
};
//++++++++++++++++++++++++++++++++++
function createDuedateModal(invoiceId, invoiceDuedate, invoiceDuedateReverse) {
    var modalHTML = '' +
        // '<div class="modal fade" id="duedate-'+ invoiceId +'" role="dialog">' +
            // '<div class="modal-dialog">' +
                '<div class="modal-content">' +
                    '<div class="modal-header">' +
                       '<button type="button" class="btn" data-dismiss="modal"><span class="line">Отменить перенос</span></button>' +
                        // '<h4 class="modal-title">Изменение срока оплаты счета №'+ invoiceId +'</h4>' +
                    '</div>' +
                    '<div class="modal-body text-center">' +
                        // '<input class="text-center duedate-input" type="text" value="'+ invoiceDuedate +'" disabled="disabled">' +
                        // '<span> &#8594; </span>' +
                        '<div class="text-center datepicker-here duedate-input duedate-input-'+ invoiceId +'"></div>' +
                        // '<input type="text" class="text-center datepicker-here duedate-input duedate-input-'+ invoiceId +'" data-invoiceid="'+ invoiceId +'" readonly="readonly"><br/>' +
                        '<textarea class="duedate-textarea duedate-textarea-'+ invoiceId +'" data-invoiceid="'+ invoiceId +'" rows="2" placeholder="Введите ваш комментарий..."></textarea>' +
                    '</div>' +
                    '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-success saveDuedate saveDuedate-'+ invoiceId +'" data-dismiss="modal" data-duedate="'+ invoiceDuedate +'" data-invoiceid="'+ invoiceId +'" disabled="disabled">Перенести</button>' +

                    '</div>' +
                // '</div>' +
            // '</div>' +
        '</div>';

    // $('div.modals').append(modalHTML);
    invoiceItems[invoiceId] = modalHTML;
}

// $("#deal-list").on("click", ".report-date", function (e) {
//     e.preventDefault();
//     var invoiceId = $(this).parents('.pre-payed')
//                            .find(".edit-duedates")
//                            .data('invoiceid');
//     $("#duedate-"+invoiceId).modal();
// });

// $("#deal-list").on("click", ".edit-duedates", function () {
//     var invoiceId = $(this).data('invoiceid');
//     $("#duedate-"+invoiceId).modal();
// });

$(document).on("input", ".duedate-textarea", function () {
    var invoiceId = $(this).data('invoiceid');
    var inputVal = $('.duedate-input-'+invoiceId).val();
    var textareaVal = $('.duedate-textarea-'+invoiceId).val();
    if (inputVal !== '' && textareaVal !== '') {
        $('.saveDuedate-'+invoiceId).prop('disabled', false);
    } else {
        $('.saveDuedate-'+invoiceId).prop('disabled', true);
    }
});

$(document).on("blur", ".duedate-input, .datepicker-here", function () {
    var invoiceId = $(this).data('invoiceid');
    var inputVal = $('.duedate-input-'+invoiceId).val();
    var textareaVal = $('.duedate-textarea-'+invoiceId).val();
    if (inputVal !== '' && textareaVal !== '') {
        $('.saveDuedate-'+invoiceId).prop('disabled', false);
    } else {
        $('.saveDuedate-'+invoiceId).prop('disabled', true);
    }
});

$(document).on("click", "button.saveDuedate", function () {
    var invoiceId = $(this).data('invoiceid');
    var curDatetime = tempus().format('%d.%m.%Y %H:%M');
    var oldDuedate = $(this).data('duedate');
    var newDuedateReverse = tempus($('.duedate-input-'+invoiceId).val()).format('%Y-%m-%d');
    var newDuedate = new Date(newDuedateReverse);
    newDuedate.setHours(5);
    var comment = $('.duedate-textarea-'+invoiceId).val();

    var fields = {
        DATE_PAY_BEFORE: newDuedate
    };

    if (oldDuedate !== tempus(newDuedate).format('%d.%m.%Y')) {
        BX24.callMethod(
            'crm.invoice.update',
            {"id": invoiceId, "fields": fields},
            function(result)
            {

                if(result.error()){
                    console.error(result.error());
                    var errorData = result.error();
                    alert('Ошибка: ' + errorData.ex.error_description.replace(/<[^>]+>/g,''));
                }
                else
                {
                    BX24.callMethod('entity.item.add', {
                        ENTITY: 'duedates',
                        NAME: invoiceId,
                        PROPERTY_VALUES: {
                            date: curDatetime,
                            duedateAfter: tempus(newDuedate).format('%d.%m.%Y'),
                            duedateBefore: oldDuedate,
                            user: currentUserName,
                            comment: comment,
                        },
                    }, function (result) {
                        if(result.error())
                            console.error(result.error());
                        else
                        {
                            location.reload();
                        }
                    });
                }
            }
        );
    }
});

// сохранить корректировку
$('body').on("click", "button.saveCorr", function () {
    var dateCorr = $(this).data('date');
    var input = $('div.modal-body[data-date="'+dateCorr+'"]').find("input");
    var sumCorr = parseInt(input.val());
    if (isNaN(sumCorr) && sumCorr != 0) {
        sumCorr = '';
    }

    var sumCorrBefore = input.data("sumcorrbefore");
    var datetime = tempus().format("%d.%m.%Y %H:%M:%S");

    // дата воскресенья на обрабатываемой неделе
    var curDate = new Date(dateCorr);
    var CurDay = curDate.getDay();
    var curWeekSundayDate = dateCorr;

    if (CurDay != 0) {
        var daysToSunday = 7 - parseInt(CurDay);
        curDate.setDate(curDate.getDate() + daysToSunday);
        curWeekSundayDate = tempus(curDate).format('%Y.%m.%d');
    }

    var corrId = arCorrectionsIds[curWeekSundayDate];

    var arCommands = [];
    var valueCorr = 0;
    var objQuery = {};

    if (!arHistories.hasOwnProperty(curWeekSundayDate)) {
        arHistories[curWeekSundayDate] = {};
    }

    if (!arHistories[curWeekSundayDate].hasOwnProperty(dateCorr)) {
        arHistories[curWeekSundayDate][dateCorr] = {};
    }

    arHistories[curWeekSundayDate][dateCorr][datetime] = {
        username: currentUserName,
        sum_before: sumCorrBefore,
        sum_after: sumCorr,
    };

    arCorrections[curWeekSundayDate][dateCorr] = sumCorr;
    arFullCorrections[curWeekSundayDate]['corrSum'] = arCorrections[curWeekSundayDate];

    var corrSums = JSON.stringify(arFullCorrections[curWeekSundayDate]);
    var corrHistory = JSON.stringify(arHistories[curWeekSundayDate]);

    if (corrId) {
        // update
        objQuery = {
            method: 'entity.item.update',
            params: {
                ENTITY: 'corrections',
                ID: corrId,
                PREVIEW_TEXT: corrSums, // сериализованный массив корректировок за неделю
                DETAIL_TEXT: corrHistory, // сериализованный массив истории корректировок за неделю
            }
        };
    } else {
        // add
        objQuery = {
            method: 'entity.item.add',
            params: {
                ENTITY: 'corrections',
                NAME: curWeekSundayDate,
                PREVIEW_TEXT: corrSums, // сериализованный массив корректировок за неделю
                DETAIL_TEXT: corrHistory, // сериализованный массив истории корректировок за неделю
            }
        };
    }

    arCommands.push(objQuery);

    // колбэк перезагружает страницу
    var batchCallback = function(batchResult) {
        BX24.appOption.set("restart", "Y", function () {
            console.log('status: '+BX24.appOption.get("restart"));
            BX24.reloadWindow();
        });
    };

    BX24.callBatch(arCommands, batchCallback);
});

// обработка нажатия переключения на другую неделю
application.prototype.changeWeek = function (prevDays) {
    $("#week").fadeOut(200, 'swing', function() {
        // удаляем строки со счетами
        $("tr.invoice-tr").remove();

        // удаляем строки с менеджерами
        $("tr.manager").remove();

        // очищаем даты и суммы сверху таблицы
        $('.dates-td').empty();

        // удаляем строки с суммами и датами
        $( "#deal-sums > .td-days" ).empty();
        $( "#deal-dates" ).empty();
        $("tr#deal-income,tr#deal-expense").empty();
        $("tr#balance-begin,tr#balance-corr,tr#balance-end").empty();
        $('tr#deal-expense').hide();
        $('tr#deal-income').hide();

        mondayDate.setDate(mondayDate.getDate()-prevDays);
        sundayDate.setDate(sundayDate.getDate()-prevDays);

        sumFlag = false;
        prevCount = 1;
        invoicesCount = 0;
        managersCount = 0;
        thisWeekSumObj = {
            incomes: {
                sumPayed: 0,
                sumNotPayed: 0,
            },
            expenses: {
                sumPayed: 0,
                sumNotPayed: 0,
            },
        };
        fullSumDays = [0, 0, 0, 0, 0, 0, 0];

        app.displayDates(mondayDate, sundayDate, '#deal-dates');

        chain(function(next) {
            app.findAllInvoices(mondayDate, sundayDate, '#deal-list', '#deal-sums');
            next();
        }).then(function(next) {
            prevCount = 1;
            app.findAllInvoicesPrev(prevArguments[prevCount]['prevDays'], mondayDate, sundayDate, prevArguments[prevCount]['dates'], prevArguments[prevCount]['sums']);
            next();
        });

        /*app.findAllInvoices(mondayDate, sundayDate, '#deal-list', '#deal-sums');
        app.findAllInvoicesPrev(7, mondayDate, sundayDate, '#dates-prev-1', '#sums-prev-1');
        app.findAllInvoicesPrev(14, mondayDate, sundayDate, '#dates-prev-2', '#sums-prev-2');
        app.findAllInvoicesPrev(21, mondayDate, sundayDate, '#dates-prev-3', '#sums-prev-3');

        app.findAllInvoicesPrev(-7, mondayDate, sundayDate, '#dates-next-1', '#sums-next-1');
        app.findAllInvoicesPrev(-14, mondayDate, sundayDate, '#dates-next-2', '#sums-next-2');
        app.findAllInvoicesPrev(-21, mondayDate, sundayDate, '#dates-next-3', '#sums-next-3');*/
    });
    BX24.fitWindow();
};

var date2str = function(d)
{
    return d.getFullYear() + '-' + paddatepart(1 + d.getMonth()) + '-' + paddatepart(d.getDate()) + 'T' + paddatepart(d.getHours())
        + ':' + paddatepart(d.getMinutes()) + ':' + paddatepart(d.getSeconds()) + '+03:00';
};
var paddatepart = function(part)
{
    return part >= 10 ? part.toString() : '0' + part.toString();
};

// promises function
function chain(callback) {
    var queue = [];

    function _next() {
        var cb = queue.shift();
        if (cb) {
            cb(_next);
        }
    }

    setTimeout(_next, 0);

    var then = function(cb) {
        queue.push(cb);
        return { then: then }
    }

    return then(callback);
}

// create our application
app = new application();


//Скрытие окон по нажатию на область экрана
$(document).mouseup(function (e){ // событие клика по веб-документу
    var el = $('td[data-invoiceid] .tooltip-inner');
    if (el.length) {
        var attr = el.parents('td[data-invoiceid]').attr('data-invoiceid');
        var span = $('td[data-invoiceid="'+attr+'"] span'); // тут указываем ID элемента
        var tooltip = $('td[data-invoiceid="'+attr+'"] .tooltip'); // тут указываем ID элемента
        if (!span.is(e.target) // если клик был не по нашему блоку
            && span.has(e.target).length === 0
            && !tooltip.is(e.target) // если клик был не по нашему блоку
            && tooltip.has(e.target).length === 0) { // и не по его дочерним элементам
            span.tooltip('hide'); // скрываем его
        }

    }
});


//Переключение между комментарием и календарем
$(document).on('click', '.tooltip-inner a.report-date', function(e){
    // console.log('report-date');
    e.preventDefault();
    var parent = $(this).parents('.tooltip-inner')
    parent.find('> *:not(.modal-content)').hide();
    parent.find('.modal-content').show();


    BX24.fitWindow();

});

$(document).on('click', '.tooltip-inner span.line', function(e){
    // console.log('span.line');
    e.preventDefault();
    var parent = $(this).parents('.tooltip-inner')
    parent.find('> *:not(.modal-content)').show();
    parent.find('.modal-content').hide();


    BX24.fitWindow()
});


